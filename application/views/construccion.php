
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Iknalmuuk</title>

    <link rel="canonical" href="<?php echo base_url();?>">

    <!-- Bootstrap core CSS -->
<link href="<?php echo base_url();?>public/css/bootstrap.css" rel="stylesheet" >
<link href="<?php echo base_url();?>public/css/animate.css" rel="stylesheet" >
<meta name="theme-color" content="#47bbe8">
    <!-- Favicons -->
<link rel="icon" href="https://beaverds.com/public/images/favicon.ico" type="image/x-icon" />
        <link id="favicon" rel="shortcut icon" href="https://beaverds.com/public/images/favicon.png" type="image/png" />
        <link rel="apple-touch-icon" sizes="194x194" href="https://beaverds.com/public/images/favicon.png" type="image/png" />


<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">
<script src="<?php echo base_url();?>public/js/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>public/./vendor/three.r92.min.js"></script>
<script src="<?php echo base_url();?>public/./dist/vanta.waves.min.js"></script>
<script type="text/javascript">
  $(document).ready(function($) {
    VANTA.WAVES({
      el: "#animatedback",
      mouseControls: true,
      touchControls: true,
      minHeight: 200.00,
      minWidth: 200.00,
      scale: 1.00,
      scaleMobile: 1.00
    });
  });

</script>



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>public/css/signin.css" rel="stylesheet">
  </head>
  <body id="animatedback">
    <main role="main" class="container fadeInDown animated">
      <div class="jumbotron card text-center" >
        <div class="row">
          <div class="col-md-12">
            <p>
              <a href="https://beaverds.com">
                <img class="mb-4" src="https://beaverds.com/site/public/images/logo.svg" alt="" width="100">
              </a>
              

            <p><a href="http://beaverds.com">Beaverds</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h1>El sitio web está en construcción</h1>
          </div>
          <div class="col-md-6">
            <img src="<?php echo base_url();?>public/img/construction_website.svg" width="250">
          </div>
        </div>
        
        
        
      </div>
    </main>
    
    

</body>
</html>
