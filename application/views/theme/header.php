<!DOCTYPE html>
<html  lang="es">
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->	
		<title>IKNAL MUUK | Ingeniería y construcción</title>	
        <meta name="authoring-tool" content="iknal muuk,iknal muuk,iknal,muuk">
        <meta name="description" content="Ingeniería y construcción">
        <meta name="keywords" content="iknal muuk, iknalmuuk, beaverds">
        <meta name="author" content="iknal muuk">
        <meta name="robots" content="index"/>
        <meta name="og:description" content="iknal muuk">
        <meta name="og:url" content="https://www.iknalmuuk.com//">
        <meta name="og:title" content="iknal muuk">

        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/muuk/icon_logo.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/img/muuk/icon_logo.png">

		<!-- Mobile Metas -->
		<!--<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
        -->
		<!-- Web Fonts  
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700,800%7CMontserrat:300,400,600,700" rel="stylesheet" type="text/css">
       -->
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demos/demo-architecture-interior.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/skin-architecture-interior.css"> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demos/demo-finance.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/skin-finance.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/vendor/modernizr/modernizr.min.js"></script>
    <!--
    <style type="text/css">
    
    	.sticky-wrapper.sticky-wrapper-transparent .sticky-body {
		    background: #f7f7f7;
		}
		html .text-color-light, html .text-light {
		    color: #212529 !important;
		}
		.social-icons.social-icons-icon-light.social-icons-clean li a i {
		    color: #333 !important;
		}
		.hamburguer-btn.hamburguer-btn-light .hamburguer span, .hamburguer-btn.hamburguer-btn-light .close span {
		    background: #000000 !important;
		}
		.sticky-wrapper.sticky-wrapper-effect-1.sticky-wrapper-effect-1-dark.sticky-effect-active .sticky-body {
		    background: #e6e6e6;
		}
		.active-revslide .custom-square-1 rect {
		    stroke-dashoffset: 0;
		}
    
    </style>
-->
	</head>
	<body data-target="#header" data-spy="scroll" data-offset="100">
