
		<div class="body">
			<header id="header" class="header-narrow header-semi-transparent-light" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<img class="" alt="Porto" width="280" height="80" src="<?php echo base_url(); ?>assets/img/muuk/logo.jpeg">
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-stripe order-2 order-lg-1">
										<div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a class="nav-link active" href="#inicio">
															Inicio
														</a>
													</li>
													<li ><!-- class="dropdown" -->
														<a class="nav-link dropdown-toggle"  href="#servicios_id">
															Services
														</a>
														<!--
														<ul class="dropdown-menu">
															<li><a class="dropdown-item" href="demo-construction-services-detail.html">Pre-Construction</a></li>
															<li><a class="dropdown-item" href="demo-construction-services-detail.html">General Construction</a></li>
															<li><a class="dropdown-item" href="demo-construction-services-detail.html">Plumbing</a></li>
															<li><a class="dropdown-item" href="demo-construction-services-detail.html">Painting</a></li>
														</ul>
													    -->
													</li>
													<li>
														<a class="nav-link" href="#footer">
															Contactos
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
									<div class="header-nav-features header-nav-features-no-border d-none d-sm-block order-1 order-lg-2">
										<ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean ml-0">
											<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
											<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main" id="inicio">
				<div class="slider-container light rev_slider_wrapper" style="height: 700px;">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 700, 'disableProgressBar': 'on'}">
						<ul>
							<li data-transition="fade">
								<img src="<?php echo base_url(); ?>assets/img/muuk/tubos.jpeg"  
									alt=""
									data-bgposition="center 100%" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">
								
								<div class="tp-caption top-label tp-caption-custom-stripe"
									data-x="right" data-hoffset="100"
									data-y="bottom" data-voffset="106"
									data-start="1000"
									data-transform_in="x:[100%];opacity:0;s:1000;">Construcción </div>
							</li>
							<li data-transition="fade">
								<img src="<?php echo base_url(); ?>assets/img/muuk/instalcion2.jpeg"  
									alt=""
									data-bgposition="center 100%" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">
								
								<div class="tp-caption top-label tp-caption-custom-stripe"
									data-x="right" data-hoffset="180"
									data-y="bottom" data-voffset="106"
									data-start="1000"
									data-transform_in="x:[100%];opacity:0;s:1000;">Mantenimiento</div>
							</li>
						</ul>
					</div>
				</div>

				<div class="container">
					<div class="row mt-4 mt-lg-5 mb-4 py-4">
						<div class="col-lg-2"></div>
						<div class="col-lg-2">
							<h2 class="mb-0 text-color-dark">Misión:</h2>
						</div>
						<div class="col-lg-2 text-center d-none d-lg-block">
							<img src="<?php echo base_url(); ?>assets/img/dotted-line-angle.png" class="img-fluid" alt="" />
						</div>
						<div class="col-lg-6">
							<p>La búsqueda permanente de mejora contínua, inspirada en la innovación del diseño y ejecución de cada uno de los proyectos de ingeniería, procura, construcción y mantenimiento, logrando un desarrollo integral y de alto valor agregado para nuestros clientes.</p>
						</div>
					</div>
				</div>
                <div class="container">
					<div class="row mt-4 mt-lg-5 mb-4 py-4">
						<div class="col-lg-2"></div>
						<div class="col-lg-2">
							<h2 class="mb-0 text-color-dark">Visión:</h2>
						</div>
						<div class="col-lg-2 text-center d-none d-lg-block">
							<img src="<?php echo base_url(); ?>assets/img/dotted-line-angle.png" class="img-fluid" alt="" />
						</div>
						<div class="col-lg-6">
							<p>Impulsar la innovación, eficiencia y rendimiento sobresaliente en todos nuestros proyectos, satisfaciendo las necesidades de nuestros clientes por encima de sus expectativas y estableciendo bases firmes para ser una empresa competitiva y comprometida con la seguridad, el medio ambiente, la salud ocupacional de nuestros trabajadores y nuestra responsabilidad social.</p>
						</div>
					</div>
				</div>
				<section class="section section-tertiary section-no-border section-custom-construction" id="servicios_id">
					<div class="container">
						<div class="row pt-4">
							<div class="col">
								<h2 class="mb-0 text-color-dark">Servicios</h2>
								<p class="lead">Servicios de Ingeniería básica y de detalle de las diferentes disciplinas:</p>
							</div>
						</div>

						<div class="row mt-4">
							<div class="col-lg-6">
								<div class="feature-box feature-box-style-2 custom-feature-box mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
									<div class="feature-box-icon w-auto">
										<img src="<?php echo base_url(); ?>assets/img/demos/construction/icons/ground-construction.png" alt="" class="img-fluid" width="55" />
									</div>
									<div class="feature-box-info ml-3">
										<h4 class="mb-2">Ingeniería básica y de detalle</h4>
										<p>Procesos
											,Tuberías 
											,Mecánica 
											,Instrumentación 
											,Eléctrica 
											,Flexibilidad
											,Seguridad Industrial
											,Civil Estructural
											,Civil Concreto
											y Telecomunicaciones
											</p>
									<!--	<a class="mt-3" href="demo-construction-services-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a> -->
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="feature-box feature-box-style-2 custom-feature-box mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
									<div class="feature-box-icon w-auto">
										<img src="<?php echo base_url(); ?>assets/img/demos/construction/icons/construction.png" alt="" class="img-fluid" width="55" />
									</div>
									<div class="feature-box-info ml-3">
										<h4 class="mb-2">Procura de materiales:</h4>
										<p>Ingeniería de materiales
											,Abastecimiento 
											y Expeditación
											</p>
										<!--<a class="mt-3" href="demo-construction-services-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>-->
									</div>
								</div>
							</div>
						</div>

						<div class="row mt-3 mb-4">
							<div class="col-lg-6">
								<div class="feature-box feature-box-style-2 custom-feature-box mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
									<div class="feature-box-icon w-auto">
										<img src="<?php echo base_url(); ?>assets/img/demos/construction/icons/painting.png" alt="" class="img-fluid" width="55" />
									</div>
									<div class="feature-box-info ml-3">
										<h4 class="mb-2">Construcción e Instalación </h4>
										<p>Gerencia de proyecto
											,Asistencia técnica
											y Supervisión de obra
											</p>
										<!--<a class="mt-3" href="demo-construction-services-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a> -->
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="feature-box feature-box-style-2 custom-feature-box mb-4 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
									<div class="feature-box-icon w-auto">
										<img src="<?php echo base_url(); ?>assets/img/demos/construction/icons/plumbing.png" alt="" class="img-fluid" width="55" />
									</div>
									<div class="feature-box-info ml-3">
										<h4 class="mb-2">Mantenimiento </h4>
										<p>Soldadura 
											,Pailería
											,Maniobra 
											,Tubería 
											,Doblado y Rolado
											,Ductería
											,Instalaciones eléctricas y de control
											y Obra civil
											</p>
										<!--<a class="mt-3" href="demo-construction-services-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="pt-3 pb-4 home-concept-construction">
					<div class="container">
						<div class="row pt-4">
							<div class="col">
								<h2 class="mb-0 text-color-dark">Ingeniería</h2>
								<!--<p class="lead">Lorem ipsum dolor sit amet.</p>-->

								<div class="diamonds-wrapper lightbox" data-plugin-options="{'delegate': '.diamond', 'type': 'image', 'gallery': {'enabled': true}}">
									<ul class="diamonds">
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/tanques.jpeg" class="diamond">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/tanques.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/tuberia.jpeg" class="diamond">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/tuberia.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/soldadura.jpeg" class="diamond">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/soldadura.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/soldar.jpeg" class="diamond diamond-sm">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/soldar.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/tubos2.jpeg" class="diamond">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/tubos2.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/soldar2.jpeg" class="diamond diamond-sm">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/soldar2.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
										<li>
											<a href="<?php echo base_url(); ?>assets/img/muuk/caja.jpeg" class="diamond diamond-sm">
												<div class="content">
													<img src="<?php echo base_url(); ?>assets/img/muuk/caja.jpeg" class="img-fluid" alt="" />
												</div>
											</a>
										</li>
									</ul>
								</div>

							</div>
						</div>
						<div class="row row-diamonds-description justify-content-center justify-content-xl-start text-center text-xl-left">
							<div class="col-md-8 col-lg-6">

								<p>Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
								<a class="btn btn-outline btn-primary" href="demo-construction-projects.html">View All Projects</a>
							
							</div>
						</div>
					</div>
				</section>

               <!--
				<section class="pt-3 section-custom-construction-2">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<div class="owl-carousel owl-theme stage-margin rounded-nav" data-plugin-options="{'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40, 'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-1.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-2.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-3.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-4.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-5.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-6.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-4.png" alt="">
									</div>
									<div>
										<img class="img-fluid" src="<?php echo base_url(); ?>assets/img/logos/logo-2.png" alt="">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 text-center mt-4">
								<hr class="solid mt-0 mb-4">
							</div>
						</div>
						<div class="row pt-4">
							<div class="col">
								<h2 class="mb-0 text-color-dark">Blog</h2>
								<p class="lead mb-2">Lorem ipsum dolor sit amet.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="recent-posts mt-4">
									<a href="demo-construction-blog-detail.html">
										<img class="img-fluid pb-3" src="<?php echo base_url(); ?>assets/img/demos/construction/blog/blog-construction-1.jpg" alt="Blog">
									</a>
									<article class="post">
										<div class="post-date">
											<span class="day">15</span>
											<span class="month">Jan</span>
										</div>
										<h4 class="pt-2 pb-0 mb-0"><a class="text-color-dark" href="demo-construction-blog-detail.html">Lorem ipsum dolor sit amet</a></h4>
										<p>By admin</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.</p>
										<a class="mt-3" href="demo-construction-blog-detail.html">Read More <i class="fas fa-long-arrow-alt-right position-relative top-1"></i></a>
									</article>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="recent-posts mt-4">
									<a href="demo-construction-blog-detail.html">
										<img class="img-fluid pb-3" src="<?php echo base_url(); ?>assets/img/demos/construction/blog/blog-construction-2.jpg" alt="Blog">
									</a>
									<article class="post">
										<div class="post-date">
											<span class="day">15</span>
											<span class="month">Jan</span>
										</div>
										<h4 class="pt-2 pb-0 mb-0"><a class="text-color-dark" href="demo-construction-blog-detail.html">Lorem ipsum dolor sit amet</a></h4>
										<p>By admin</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.</p>
										<a class="mt-3" href="demo-construction-blog-detail.html">Read More <i class="fas fa-long-arrow-alt-right position-relative top-1"></i></a>
									</article>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="recent-posts mt-4">
									<a href="demo-construction-blog-detail.html">
										<img class="img-fluid pb-3" src="<?php echo base_url(); ?>assets/img/demos/construction/blog/blog-construction-3.jpg" alt="Blog">
									</a>
									<article class="post">
										<div class="post-date">
											<span class="day">15</span>
											<span class="month">Jan</span>
										</div>
										<h4 class="pt-2 pb-0 mb-0"><a class="text-color-dark" href="demo-construction-blog-detail.html">Lorem ipsum dolor sit amet</a></h4>
										<p>By admin</p>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.</p>
										<a class="mt-3" href="demo-construction-blog-detail.html">Read More <i class="fas fa-long-arrow-alt-right position-relative top-1"></i></a>
									</article>
								</div>
							</div>
						</div>
					</div>
				</section>
			   -->	
			</div>

			<footer id="footer">
				<div class="container">
					<div class="row pt-4 pb-5 text-center text-md-left">
						<div class="col-md-4">
							<div class="row">
								<div class="col-lg-6 mb-2">
									<h4>Navegación</h4>
								</div>
							</div>
							<div class="row">
								<div class="col-6 mb-0">
									<ul class="list list-footer-nav">
										<li>
											<a href="demo-construction.html">
												Inicio
											</a>
										</li>
										<li>
											<a href="demo-construction-company.html">
												Servicios
											</a>
										</li>
										<li>
											<a href="demo-construction-services.html">
												Contacto
											</a>
										</li>
									</ul>
								</div>
								<div class="col-6">
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<h4>Para mas información puedes comunicarte con los siguientes medios</h4>
						</div>
						<div class="col-md-3">
							<h4>EMail</h4>
							<p><i class="far fa-envelope ml-2"></i> <a href="mailto:mail@example.com">ventas@iknalmuuk.com</a></p>
							<h4>Teléfono</h4>
							<p><i class="fas fa-phone"></i> (123) 456-789</p>
						</div>
					</div>

					<div class="footer-copyright">
						<div class="row">
							<div class="col-lg-12 text-center">
								<p>© Copyright 2020. Todos los derechos reservados.</p>
							</div>
						</div>
					</div>

				</div>
			</footer>

		</div>

