<style type="text/css">
	.custom-square-1 rect {
    stroke: #9a111e !important;
    fill: #9a111e !important;
    }
    .custom-featured-boxes-style-1 .featured-box:hover .box-content {
	    border: 7px solid #9a111e !important;
	}
	.custom-featured-boxes-style-1 .featured-box:hover .box-content svg{
	    fill: #9a111e !important;
	}
	html .text-color-primary, html .text-primary {
	    color: #9a111e !important;
	}
    html .btn-outline.btn-primary {
	    color: #9a111e !important;
	    background-color: transparent;
	    background-image: none;
	    border-color: #9a111e !important;
	}
	html .btn-outline.btn-primary:hover, html .btn-outline.btn-primary.hover {
    color: #777 !important;
	    background-color: #9a111e !important;
	    border-color: #9a111e !important;
	}
	.custom-thumb-info-style-1 .thumb-info-wrapper:after {
	    background: #9a111e !important;
	}
</style>
<div class="body">
			<div class="sticky-wrapper sticky-wrapper-transparent sticky-wrapper-effect-1 sticky-wrapper-effect-1-dark sticky-wrapper-border-bottom" data-plugin-sticky data-plugin-options="{'minWidth': 0, 'stickyStartEffectAt': 100, 'padding': {'top': 0}}">
				<div class="sticky-body" style="background: #f7f7f7">
					<div class="container container-xl">
						<div class="row justify-content-between align-items-center">
							<div class="col-auto">
								<div class="py-4">
									<a href="demo-architecture-interior.html">
										<img alt="Porto" width="200" height="58" src="<?php echo base_url(); ?>assets/img/muuk/logo_i.svg">
									</a>
								</div>
							</div>
							<div class="col-auto text-right d-flex align-items-center justify-content-end">
								<a href="tel:+1234567890" class="text-color-light font-weight-bold text-decoration-none d-none d-sm-block mr-2" style="color: #212529 !important">Contacto <span class="font-weight-light">55 48997623</span></a>
								<ul class="social-icons social-icons-clean social-icons-icon-light d-none d-md-inline-block mx-4">
									<li class="social-icons-facebook"><a href="https://www.facebook.com/IKNALMUUK/" target="_blank" title="Facebook"><i class="fab fa-facebook-f" style="color: #333 !important; font-size: 25px;"></i></a></li>
									<li class="social-icons-instagram"><a href="https://www.instagram.com/iknalmuuk/" target="_blank" title="Instagram"><i class="fab fa-instagram" style="color: #333 !important; font-size: 25px;"></i></a></li>
								</ul>
								<button class="hamburguer-btn hamburguer-btn-light" data-set-active="false">
									<span class="hamburguer">
										<span style="background: #000000 !important"></span>
										<span style="background: #000000 !important"></span>
										<span style="background: #000000 !important"></span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<header id="header" class="side-header-overlay-full-screen side-header-hide" data-plugin-options="{'stickyEnabled': false}">

				<button class="hamburguer-btn hamburguer-btn-light hamburguer-btn-side-header hamburguer-btn-side-header-overlay active" data-set-active="false">
					<span class="close">
						<span></span>
						<span></span>
					</span>
				</button>

				<div class="header-body d-flex h-100">
					<div class="header-column flex-row flex-lg-column justify-content-center h-100">
						<div class="header-container container d-flex h-100">
							<div class="header-row header-row-side-header flex-row h-100 pb-5">
								<div class="side-header-scrollable scrollable colored-slider h-50" data-plugin-scrollable>
									<div class="scrollable-content">
										<div class="header-nav header-nav-light-text header-nav-links header-nav-links-side-header header-nav-links-vertical header-nav-links-vertical-expand header-nav-click-to-open align-self-start">
											<div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-4 header-nav-main-sub-effect-1">
												<nav>
													<ul class="nav nav-pills" id="mainNav">
														<li class="dropdown">
															<a class="dropdown-item active" data-hash data-hash-offset="95" href="#home">Inicio</a>
														</li>
														<li class="dropdown">
															<a class="dropdown-item" data-hash data-hash-offset="95" href="#historia">Nuestra Historia</a>
														</li>
														<li class="dropdown">
															<a class="dropdown-item" data-hash data-hash-offset="95" href="#ourservices">Servicios</a>
														</li>
														<li class="dropdown">
															<a class="dropdown-item" data-hash data-hash-offset="95" href="#contactus">Contacto</a>
														</li>
													</ul>
												</nav>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">
				
				<div class="slider-container rev_slider_wrapper" style="height: 100vh;" id="home">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1630, 'gridheight': 800, 'responsiveLevels': [4096,1200,992,500], 'parallax': { 'type': 'mouse', 'origo': 'enterpoint', 'speed': 1000, 'levels': [2,3,4,5,6,7,8,9,12,50], 'disable_onmobile': 'on' }}">
						<ul>
							<li class="slide-overlay" data-transition="fade">
								<img src="<?php echo base_url(); ?>assets/img/tanque.jpeg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<div class="tp-caption rs-parallaxlevel-4"
									data-frames='[{"from":"opacity:0;","speed":300,"to":"opacity:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"o:0;","ease":"Power2.easeInOut"}]'
									data-x="center" data-hoffset="['-150','-150','-150','-150']"
									data-y="center" data-voffset="['-20','-20','-20','-20']"
									data-width="['430','430','630','830]"
									data-height="['330','330','530','730']">
										<svg  class="custom-square-1 custom-transition-1 custom-mobile-square-thickness" width="100%" height="100%">
											<rect style="fill: #c50c1e;" width="100%" height="100%" fill="none" stroke-width="40" stroke="#000" />
										</svg>
									</div>

								<h1 class="tp-caption font-weight-bold text-color-light ws-normal rs-parallaxlevel-3"
									data-frames='[{"from":"opacity:0;y:[50%];","speed":2000,"to":"opacity:1;","delay":800,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"o:0;","ease":"Power2.easeInOut"}]'
									data-x="center" data-hoffset="['0','0','30','30']"
									data-y="center" data-voffset="['-55','-55','-85','-120']"
									data-width="['580','580','780','1000']"
									data-fontsize="['66','66','86','120']"
									data-lineheight="['72','72','90','125']">Innovación de Ingeniería y Construcción</h1>
							</li>
							<li class="slide-overlay" data-transition="fade">
								<img src="<?php echo base_url(); ?>assets/img/muuk/instalacion2.jpeg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">

								<h2 style="color: white !important;" class="tp-caption text-center font-weight-bold text-color-light ws-normal rs-parallaxlevel-3"
									data-frames='[{"from":"opacity:0;y:[50%];","speed":2000,"to":"opacity:1;","delay":800,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"o:0;","ease":"Power2.easeInOut"}]'
									data-x="center" data-hoffset="['0','0','30','30']"
									data-y="center" data-voffset="['-55','-55','-85','-120']"
									data-width="['580','580','780','1000']"
									data-fontsize="['66','66','86','120']"
									data-lineheight="['72','72','90','125']">Desarrollo y mejora continua de Técnicas de Ingeniería</hc2>

								<a class="tp-caption d-inline-flex align-items-center btn btn-dark font-weight-bold rounded ls-0 rs-parallaxlevel-2"
									data-hash
									data-hash-offset="95"
									href="#contactus"
									data-frames='[{"delay":1600,"speed":2000,"frame":"0","from":"x:-50%;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
									data-x="['center','center','center','center']" data-hoffset="['-5','-5','25','45']"
									data-y="center" data-voffset="['125','125','210','275']"
									data-paddingtop="['20','20','30','40']"
									data-paddingbottom="['20','20','30','40']"
									data-paddingleft="['68','68','68','95']"
									data-paddingright="['15','15','15','25']"
									data-fontsize="['16','16','23','45']"
									data-lineheight="['20','20','26','50']">CONTACTO<i class="fas fa-arrow-right ml-4 pl-3 mr-2 text-4"></i></a>

							</li>
						</ul>
					</div>
				</div>

				<section class="section section-height-5 bg-light border-0 m-0" id="historia">
					<div class="container container-xl">
						<div class="row align-items-center">
							<div class="col-lg-6 mb-5 mb-lg-0">
								<div class="overflow-hidden mb-3">
									<h2 style="color: #9a111e !important;" class="font-weight-extra-bold text-11 negative-ls-1 line-height-3 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">Nuestra Historia:</h2>
								</div>
								<p style="color: black !important;" class="text-4 line-height-9 pr-5 pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Somos un grupo de especialistas en ingeniería y construcción, con una propuesta de valor diferenciada, ofreciendo el desarrollo de proyectos integrales en los sectores energético, industrial e hidrocarburos.</p>
							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-6 col-xl-4 order-1">
										<img src="<?php echo base_url(); ?>assets/img/muuk/tub.jpeg" class="img-fluid appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="3000" alt="" />
									</div>
									<div class="col-12 col-xl-4 order-3 order-xl-2">
										<div class="h-100 d-flex align-items-center position-relative px-4 py-4 py-xl-0 mt-4 mt-xl-0">
											<svg class="custom-square-1 custom-square-top-right z-index-0" width="80" height="80">
												<rect class="lineProgressFrom appear-animation" data-appear-animation="lineProgressTo" data-appear-animation-duration="3s" width="80" height="80" fill="none" stroke-width="13" stroke="#000" />
											</svg>
											<svg class="custom-square-1 custom-square-1-no-pos z-index-0" width="100%" height="100%">
												<rect class="lineProgressAndFillFrom appear-animation" data-appear-animation="lineProgressAndFillTo" data-appear-animation-duration="3s" width="100%" height="100%" fill="none" stroke-width="13" stroke="#000" />
											</svg>
											<p class="text-color-light line-height-9 text-center text-4 z-index-1 custom-responsive-text-size-1 mb-0 px-2">Impulsamos innovación en construcción y mantenimiento, haciendo lo posible por satisfacer a nuestros clientes, que buscan satisfacer sus nesecidades.</p>
										</div>
									</div>
									<div class="col-6 col-xl-4 order-2 order-xl-3">
										<img src="<?php echo base_url(); ?>assets/img/muuk/instalacion.jpeg" class="img-fluid appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="3000" alt="" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="section section-height-4 bg-color-grey-scale-1 border-0 m-0" id="ourblog">
					<div class="container container-xl">
						<div id="portfolioLoadMoreWrapperBlog" class="row portfolio-list sort-destination pb-2" data-sort-id="portfolio" data-total-pages="3" data-ajax-url="ajax/demo-architecture-interior-blog-ajax-load-more-">

							<div class="col-lg-4 isotope-item text-left">
								<div class="portfolio-item appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="450">
									<div class="card" style="background-color: #333 !important;">
										<div class="card-body p-5 mt-2">
											<h3 class="font-weight-bold text-transform-none line-height-4 text-5 pr-xl-5 mr-xl-5">
												<a href="demo-architecture-interior-blog-post.html" class="text-decoration-none"  style="color: white !important;">Misión</a>
											</h3>
											<p class="mb-4" style="color: white !important;">La búsqueda permanente de mejora continua, inspirada en la innovación del diseño y ejecución de cada uno de los proyectos de ingeniería, procura, construcción y mantenimiento, logrando un desarrollo integral y de alto valor agregado para nuestros clientes.</p>
			
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 isotope-item text-left">
								<div class="portfolio-item appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="250">
									<div class="card" style="background-color: #ccc !important;">
										<div class="card-body p-5 mt-2">
											<h3 class="font-weight-bold text-transform-none line-height-4 text-5 pr-xl-5 mr-xl-5">
												<a href="demo-architecture-interior-blog-post.html" class="text-decoration-none" style="color: black !important;">Visión</a>
											</h3>
											<p class="mb-4" style="color: black !important;">Impulsar la innovación, eficiencia y rendimiento sobresaliente en todos nuestros proyectos, satisfaciendo las necesidades de nuestros clientes por encima de sus expectativas y estableciendo bases firmes para ser una empresa competitiva y comprometida con la seguridad, el medio ambiente, la salud ocupacional de nuestros trabajadores y nuestra responsabilidad social.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 isotope-item text-left">
								<div class="portfolio-item appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="650">
									<div class="card" style="background-color: #818181 !important;">
										<div class="card-body p-5 mt-2">
											<h3 class="font-weight-bold text-transform-none line-height-4 text-5 pr-xl-5 mr-xl-5">
												<a href="demo-architecture-interior-blog-post.html" class="text-decoration-none" style="color: black !important;">Valores</a>
											</h3>
											<ul style="color: black !important;">
											<li class="mb-4">Respeto</li>
                                            <li class="mb-4">Comunicación</li>
                                            <li class="mb-4">Responsabilidad laboral</li>
                                            <li class="mb-4">Constancia y disciplina</li>
                                            <li class="mb-4">Integridad laboral</li>
										    </ul>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</section>
				<section style="background-color: #818181 !important;" class="section section-height-4 border-0 m-0 appear-animation" data-appear-animation="fadeIn" id="ourservices">
					<div class="container container-xl">
						<div class="row">
							<div class="col text-center">
								<div class="overflow-hidden">
									<h1 style="text-transform: uppercase; color: #9a111e" class="d-block custom-font-secondary font-weight-semibold appear-animation" data-appear-animation="maskUp">Servicios de Ingeniería básica y de detalle de las diferentes disciplinas</h1>
								</div>
							</div>
						</div>
						<div class="featured-boxes featured-boxes-style-4 custom-featured-boxes-style-1">
							<div class="row mb-2">
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
																							
												<svg height="480pt" viewBox="0 0 480 480.584" width="480pt" xmlns="http://www.w3.org/2000/svg"><path d="m89.691406 130.632812c22.773438 0 41.234375-18.460937 41.234375-41.234374 0-22.769532-18.460937-41.230469-41.234375-41.230469-22.769531 0-41.230468 18.460937-41.230468 41.230469.027343 22.761718 18.46875 41.207031 41.230468 41.234374zm0-66.464843c13.9375 0 25.234375 11.296875 25.234375 25.230469 0 13.9375-11.296875 25.234374-25.234375 25.234374-13.933594 0-25.230468-11.296874-25.230468-25.234374.015624-13.925782 11.304687-25.210938 25.230468-25.230469zm0 0"/><path d="m448.429688 355.382812-3.089844-80.71875 15.054687-20.703124-18.253906-33.554688 3.664063-11.277344 34.488281-16.417968v-33.421876l-34.488281-16.417968-3.664063-11.277344 18.253906-33.554688-19.648437-27.023437-37.550782 6.984375-9.597656-6.976562-4.960937-37.863282-31.761719-10.328125-26.277344 27.71875h-11.859375l-26.277343-27.71875-31.769532 10.328125-1.550781 11.808594-111.113281-6.617188-7.488282-7.488281-2.070312-16.800781-29.984375-12.460938-13.359375 10.4375h-10.832031l-13.394531-10.4375-30 12.445313-2.0625 16.800781-7.679688 7.679688-16.800781 2.066406-12.464844 30.015625 10.457031 13.390625v10.855469l-10.457031 13.335937 9.914063 23.914063-11.039063 141.535156c-.25 1.804687-.390625 3.621094-.425781 5.4375v.296875h-.046875v.625c-.023438 16.230469 8.171875 31.371094 21.773437 40.230469 13.601563 8.859375 30.765625 10.230469 45.601563 3.648437l47.34375-19.726562c-7.671875 21.660156 1.191406 45.714844 21.089843 57.214844l-73.128906 23.5625c-24.191406 4.824218-40.785156 27.203124-38.378906 51.753906 2.40625 24.550781 23.03125 43.277344 47.699219 43.316406h.421875l294.578125-15.449219 4.511719 10.816407 25.4375 5.21875 20.648437-20.058594 5.257813-1.742188 28.550781 3.679688 17.242187-19.425782-7.050781-27.910156 1.121094-5.433594 17.457031-22.886718-8.175781-24.640625zm-173.195313-310.136718 12.5625-4.078125 24.0625 25.382812h25.601563l24.0625-25.382812 12.570312 4.078125 4.535156 34.683594 20.71875 15.054687 34.398438-6.398437 7.753906 10.679687-16.703125 30.734375 7.917969 24.359375 31.578125 15.039063v13.203124l-31.578125 15.039063-7.917969 24.359375 16.703125 30.726562-7.753906 10.679688-34.398438-6.398438-20.71875 15.054688-4.535156 34.679688-12.570312 4.082031-24.0625-25.382813h-25.601563l-24.0625 25.382813-12.5625-4.082031-4.542969-34.664063-20.71875-15.0625-34.402344 6.398437-7.757812-10.679687 16.703125-30.71875-7.910156-24.367187-31.578125-15.039063v-13.203125l31.578125-15.039062 7.910156-24.367188-16.703125-30.71875 7.757812-10.679688 34.402344 6.398438 20.71875-15.078125zm-254.941406 22.875 5.277343-12.800782 13.035157-1.601562 15.453125-15.429688 1.601562-13.03125 12.800782-5.28125 10.34375 8.078126h21.878906l10.296875-8.054688 12.800781 5.28125 1.597656 13.023438 15.417969 15.457031 13.03125 1.597656 5.289063 12.800781-8.082032 10.34375v21.878906l8.082032 10.34375-5.289063 12.800782-13.03125 1.601562-15.457031 15.457032-1.601563 13.03125-12.796875 5.285156-10.34375-8.078125h-21.839844l-10.347656 8.078125-12.796875-5.285156-1.601562-13.03125-15.441407-15.539063-13.023437-1.597656-5.277344-12.800781 8.078125-10.34375v-21.839844zm-4 212.023437.09375-1.25c1.613281-16.484375 16.199219-28.601562 32.703125-27.160156s28.769531 15.902344 27.496094 32.421875c-1.269532 16.515625-15.601563 28.929688-32.128907 27.832031-16.53125-1.101562-29.09375-15.304687-28.164062-31.84375zm144 7.855469c17.671875 0 32 14.328125 32 32s-14.328125 32-32 32c-17.675781 0-32-14.328125-32-32s14.324219-32 32-32zm-120 144c.035156-12.429688 7.265625-23.710938 18.542969-28.9375l7.734374-2.488281c11.855469-2.105469 23.886719 2.628906 31.128907 12.246093 7.242187 9.621094 8.460937 22.492188 3.160156 33.300782-5.304687 10.808594-16.234375 17.71875-28.269531 17.878906h-.394532c-17.636718-.054688-31.902343-14.363281-31.902343-32zm69.390625 30.023438c8.722656-10.800782 12.324218-24.859376 9.871094-38.523438-2.457032-13.664062-10.730469-25.589844-22.671876-32.675781l78.136719-25.136719c16.878907-5.265625 29.496094-19.390625 32.835938-36.753906 3.339843-17.363282-3.136719-35.15625-16.855469-46.3125-13.722656-11.152344-32.464844-13.859375-48.78125-7.046875l-48.605469 20.257812c7.019531-19.976562.09375-42.191406-17.035156-54.640625-17.132813-12.445312-40.398437-12.167968-57.230469.679688l8-102.0625 3.878906.480468 7.679688 7.679688 2.066406 16.800781 30.007813 12.414063 13.359375-10.398438h10.855468l13.351563 10.398438 29.921875-12.382813 2.070312-16.800781 7.679688-7.679688 16.800781-2.074218 12.417969-29.988282-10.402344-13.355468v-10.902344l10.402344-13.359375-6.144531-14.847656 85.800781 5.078125-1.328125 10.160156-9.601563 6.96875-37.542968-6.984375-19.648438 27.023437 18.246094 33.554688-3.65625 11.253906-34.488281 16.441406v33.421876l34.480469 16.417968 3.664062 11.277344-18.246094 33.554688 19.648438 27.023437 37.542968-6.984375 9.601563 6.96875 4.957031 37.871094 31.769532 10.328125 26.28125-27.71875h11.855468l26.277344 27.71875 31.769531-10.328125 4.960938-37.863282 9.597656-6.976562 26.304687 4.894531 1.914063 50.058594-2.042969-4.890625-25.445312-5.230469-20.695313 20.046875-5.257812 1.753906-28.550781-3.679687-17.242188 19.421875 7.035156 27.914062-1.121094 5.429688-17.457031 22.882812 8.199219 24.644532 27.6875 7.875 4.152344 3.679687v.054688zm338.402344-59.128907-2.59375 12.59375 5.863281 23.253907-6.007813 6.761718-23.785156-3.046875-12.191406 4.046875-17.207032 16.695313-8.855468-1.808594-9.234375-22.125-9.597657-8.539063-23.066406-6.558593-2.863281-8.59375 14.546875-19.0625 2.582031-12.582031-5.863281-23.257813 6.007812-6.769531 23.785157 3.058594 12.199219-4.050782 17.214843-16.703125 8.855469 1.824219 9.242188 22.136719 9.597656 8.519531 23.066406 6.550781 2.855469 8.59375zm0 0"/><path d="m64.292969 280c0 8.835938-7.164063 16-16 16-8.835938 0-16-7.164062-16-16s7.164062-16 16-16c8.835937 0 16 7.164062 16 16zm0 0"/><path d="m176.292969 320c0 8.835938-7.164063 16-16 16-8.835938 0-16-7.164062-16-16s7.164062-16 16-16c8.835937 0 16 7.164062 16 16zm0 0"/><path d="m88.292969 432c0 8.835938-7.164063 16-16 16-8.835938 0-16-7.164062-16-16s7.164062-16 16-16c8.835937 0 16 7.164062 16 16zm0 0"/><path d="m400.292969 360c-22.09375 0-40 17.910156-40 40s17.90625 40 40 40c22.089843 0 40-17.910156 40-40-.027344-22.082031-17.921875-39.972656-40-40zm0 64c-13.253907 0-24-10.746094-24-24s10.746093-24 24-24c13.253906 0 24 10.746094 24 24s-10.746094 24-24 24zm0 0"/><path d="m320.292969 256c44.183593 0 80-35.816406 80-80s-35.816407-80-80-80c-44.183594 0-80 35.816406-80 80 .046875 44.164062 35.835937 79.953125 80 80zm0-144c35.34375 0 64 28.652344 64 64s-28.65625 64-64 64c-35.347657 0-64-28.652344-64-64 .039062-35.328125 28.667969-63.960938 64-64zm0 0"/><path d="m320.292969 224c26.507812 0 48-21.492188 48-48s-21.492188-48-48-48c-26.511719 0-48 21.492188-48 48 .027343 26.5 21.5 47.972656 48 48zm0-80c17.671875 0 32 14.328125 32 32s-14.328125 32-32 32c-17.675781 0-32-14.328125-32-32s14.324219-32 32-32zm0 0"/><path d="m432.292969 24c0 13.253906 10.746093 24 24 24 13.253906 0 24-10.746094 24-24s-10.746094-24-24-24c-13.253907 0-24 10.746094-24 24zm32 0c0 4.417969-3.582031 8-8 8s-8-3.582031-8-8 3.582031-8 8-8 8 3.582031 8 8zm0 0"/><path d="m224.292969 16c0 8.835938-7.164063 16-16 16-8.835938 0-16-7.164062-16-16s7.164062-16 16-16c8.835937 0 16 7.164062 16 16zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Procesos</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">
											<div class="box-content">
												<svg height="480pt" viewBox="-1 0 480 480" width="480pt" xmlns="http://www.w3.org/2000/svg"><path d="m64 264h32v-24h168c13.253906 0 24 10.746094 24 24v8h-8c-4.417969 0-8 3.582031-8 8v32c0 4.417969 3.582031 8 8 8h6.710938l-22.621094 150.816406c-.347656 2.300782.328125 4.640625 1.847656 6.40625 1.519531 1.761719 3.734375 2.777344 6.0625 2.777344h112c2.328125 0 4.542969-1.015625 6.0625-2.777344 1.519531-1.765625 2.195312-4.105468 1.847656-6.40625l-22.621094-150.816406h6.710938c4.417969 0 8-3.582031 8-8v-32c0-4.417969-3.582031-8-8-8h-8v-24c-.058594-48.578125-39.421875-87.941406-88-88h-19.054688l-13.746093-27.574219c-1.359375-2.726562-4.152344-4.441406-7.199219-4.425781h-8v-24c0-4.417969-3.582031-8-8-8h-8v-16h64c4.417969 0 8-3.582031 8-8v-32c0-4.417969-3.582031-8-8-8h-64v-8c0-13.253906-10.746094-24-24-24s-24 10.746094-24 24v8h-64c-4.417969 0-8 3.582031-8 8v32c0 4.417969 3.582031 8 8 8h64v16h-8c-4.417969 0-8 3.582031-8 8v24h-8c-3.03125 0-5.804688 1.710938-7.160156 4.425781l-13.785156 27.574219h-27.054688v-24h-32v-16h-64v160h64zm224 24h80v16h-80zm86.710938 176h-15.511719l-7.199219-64.878906-15.902344 1.757812 6.964844 63.121094h-29.597656l14.445312-86.679688-15.773437-2.640624-14.914063 89.320312h-15.933594l21.597657-144h50.226562zm-222.710938-400v-16h32v16zm80-16v16h-32v-16zm40 16h-24v-16h24zm-88-40c0-4.417969 3.582031-8 8-8s8 3.582031 8 8v8h-16zm-72 24h24v16h-24zm72 32h16v16h-16zm-16 32h48v16h-48zm-40.800781 64 39.199219 7.839844 3.136718-15.679688-29.726562-5.953125 9.136718-18.207031h86.109376l9.105468 18.207031-29.761718 5.953125 3.136718 15.679688 39.265625-7.839844h23.199219c39.746094.042969 71.957031 32.253906 72 72v24h-48v-8c-.027344-22.082031-17.917969-39.972656-40-40h-168v-48zm-47.199219-24v96h-16v-96zm-64 112v-128h32v128zm0 0"/><path d="m425.785156 370.953125c-22.234375 27.3125-17.929687 68.214844-17.738281 69.933594.417969 3.351562 2.902344 6.074219 6.199219 6.792969.578125.132812 1.164062.199218 1.753906.199218 2.777344-.019531 5.359375-1.441406 6.863281-3.773437.066407-.097657 6.402344-9.648438 37.410157-29.351563 11.585937-5.898437 18.429687-18.25 17.285156-31.203125-1.976563-11.21875-10.285156-20.269531-21.292969-23.199219-11.371094-3.023437-23.441406 1.171876-30.480469 10.601563zm22.941406 4.398437c1.125.003907 2.238282.15625 3.320313.457032 4.910156 1.257812 8.671875 5.203125 9.695313 10.167968.800781 5.222657-2.78125 10.65625-10.046876 15.269532-9.578124 5.976562-18.800781 12.507812-27.625 19.554687.757813-14.332031 5.660157-28.136719 14.113282-39.738281 2.421875-3.457031 6.328125-5.570312 10.542968-5.710938zm0 0"/><path d="m184.199219 372.199219c-10.328125 5.171875-16.929688 15.652343-17.136719 27.199219.355469 13.765624 8.613281 26.09375 21.210938 31.65625 16.660156 7.867187 32.105468 18.089843 45.855468 30.351562 2.199219 2.382812 5.613282 3.203125 8.652344 2.078125s5.101562-3.972656 5.21875-7.210937c.089844-2.320313 1.601562-57.066407-27.199219-78.671876-10.132812-8.71875-24.382812-10.824218-36.601562-5.402343zm47 66.199219c-11.3125-7.976563-23.160157-15.164063-35.464844-21.511719-7.199219-2.828125-12.128906-9.550781-12.664063-17.269531.082032-5.546876 3.238282-10.589844 8.195313-13.089844 1.964844-.964844 4.128906-1.457032 6.320313-1.449219 4.980468.257813 9.75 2.109375 13.597656 5.289063 12.921875 9.703124 18.175781 31.351562 20.015625 48.03125zm0 0"/><path d="m248 368h16v16h-16zm0 0"/><path d="m392 344h16v16h-16zm0 0"/><path d="m216 320h16v16h-16zm0 0"/><path d="m424 280h16v16h-16zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Tuberías</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="700">
											<div class="box-content">
												<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
														 width="70.077px" height="70.077px" viewBox="0 0 70.077 70.077" style="enable-background:new 0 0 70.077 70.077;"
														 xml:space="preserve">
													<g>
														<path d="M64.419,59.675c-1.385-0.818-2.977-0.917-4.387-0.446l-9.236-5.459c0.123-1.098,0.819-2.085,1.939-2.525
															c1.684-0.673,2.03-2.29,0.777-3.597c-1.252-1.312-1.151-3.342,0.222-4.523c1.368-1.185,1.182-2.826-0.438-3.65
															c-1.607-0.83-2.178-2.782-1.259-4.35c0.919-1.56,0.196-3.056-1.59-3.312c-1.793-0.262-2.968-1.93-2.607-3.705
															c0.363-1.776-0.801-2.955-2.582-2.619c-1.784,0.336-3.426-0.865-3.668-2.656c-0.232-1.799-1.718-2.534-3.292-1.639
															c-1.571,0.892-3.521,0.299-4.326-1.323c-0.803-1.625-2.448-1.843-3.646-0.477c-1.203,1.355-3.239,1.426-4.526,0.148
															c-1.289-1.268-2.916-0.942-3.606,0.733c-0.689,1.674-2.595,2.403-4.226,1.623c-1.635-0.787-3.065,0.049-3.174,1.857
															c-0.109,1.808-1.674,3.113-3.478,2.899c-1.796-0.208-2.874,1.046-2.391,2.787c0.479,1.75-0.576,3.495-2.347,3.875
															c-1.768,0.384-2.382,1.919-1.362,3.418c1.025,1.499,0.593,3.483-0.96,4.426c-1.55,0.938-1.629,2.593-0.174,3.677
															c1.456,1.081,1.691,3.104,0.526,4.491c-1.159,1.39-0.697,2.983,1.03,3.533c1.721,0.55,2.606,2.389,1.955,4.079
															c-0.645,1.701,0.308,3.054,2.119,3.011c1.814-0.036,3.244,1.412,3.181,3.226c-0.065,1.807,1.277,2.778,2.976,2.153
															c1.705-0.623,3.528,0.292,4.055,2.019c0.53,1.731,2.112,2.217,3.516,1.069c1.407-1.14,3.429-0.871,4.489,0.596
															c1.059,1.465,2.719,1.41,3.676-0.125c0.96-1.538,2.957-1.943,4.441-0.901c1.478,1.038,3.023,0.447,3.435-1.313
															c0.408-1.764,2.167-2.79,3.901-2.286c1.74,0.507,3.013-0.554,2.828-2.356c-0.098-0.926,0.208-1.783,0.769-2.408l9.499,5.611
															c0.273,1.464,1.133,2.812,2.514,3.629c2.531,1.501,5.808,0.661,7.309-1.88C67.79,64.449,66.955,61.179,64.419,59.675z
															 M42.886,49.096l-6.87-4.062c-0.051-2.211-1.193-4.344-3.235-5.55c-2.235-1.323-4.917-1.157-6.963,0.166l3.796,2.245
															c1.345,0.794,1.768,2.568,0.938,3.962c-0.824,1.398-2.579,1.887-3.927,1.093l-3.795-2.245c-0.175,2.423,0.974,4.856,3.214,6.18
															c2.039,1.209,4.457,1.177,6.421,0.157l6.629,3.919c-1.789,1.704-4.02,3.002-6.58,3.657c-7.979,2.055-16.135-2.771-18.185-10.746
															c-2.051-7.979,2.772-16.137,10.751-18.185c7.979-2.048,16.136,2.772,18.187,10.752C44.03,43.409,43.839,46.399,42.886,49.096z
															 M46.209,51.055l-1.387-0.821c1.22-3.2,1.51-6.783,0.593-10.349c-2.352-9.162-11.726-14.702-20.889-12.352
															C15.361,29.887,9.82,39.262,12.174,48.425c2.357,9.161,11.726,14.701,20.889,12.348c3.161-0.816,5.874-2.473,7.996-4.651
															l1.407,0.828c-2.354,2.521-5.42,4.432-9.007,5.354c-10.025,2.58-20.242-3.458-22.819-13.485C8.062,38.79,14.104,28.574,24.129,26
															c10.025-2.577,20.246,3.459,22.819,13.49C47.979,43.484,47.621,47.496,46.209,51.055z M64.12,65.699l-2.452,1.378l-2.416-1.426
															l0.03-2.811l2.449-1.377l2.418,1.423L64.12,65.699z M41.143,15.526l1.243,0.782c1.036,0.651,1.916,2.184,1.96,3.406l0.055,1.469
															c0.043,1.222,1.056,2,2.246,1.729l1.436-0.327c1.19-0.271,2.899,0.189,3.793,1.021l1.079,1.002
															c0.892,0.832,2.158,0.667,2.811-0.367l0.782-1.246c0.647-1.035,2.183-1.915,3.405-1.959l1.47-0.054
															c1.218-0.044,1.995-1.055,1.729-2.247L62.819,17.3c-0.271-1.192,0.189-2.899,1.024-3.793l1.002-1.08
															c0.832-0.894,0.668-2.158-0.367-2.812l-1.246-0.782c-1.031-0.652-1.911-2.184-1.959-3.406l-0.051-1.47
															c-0.048-1.221-1.056-2-2.246-1.728l-1.438,0.324c-1.19,0.271-2.899-0.188-3.793-1.02L52.667,0.53
															c-0.895-0.831-2.158-0.668-2.812,0.366l-0.783,1.246c-0.652,1.035-2.185,1.915-3.405,1.959l-1.469,0.054
															c-1.223,0.044-2.001,1.056-1.729,2.248l0.324,1.434c0.271,1.191-0.185,2.899-1.02,3.793l-1.002,1.079
															C39.945,13.611,40.109,14.877,41.143,15.526z M52.811,6.542c3.335,0,6.031,2.701,6.031,6.031c0,3.332-2.696,6.031-6.031,6.031
															c-3.33,0-6.031-2.7-6.031-6.031C46.779,9.245,49.482,6.542,52.811,6.542z"/>
													</g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
											    </svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Mecaníca</h3>
											</div>
										</div>
                                    </a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900">
											<div class="box-content">
												<svg id="Layer_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m272.066 512h-32.133c-25.989 0-47.134-21.144-47.134-47.133v-10.871c-11.049-3.53-21.784-7.986-32.097-13.323l-7.704 7.704c-18.659 18.682-48.548 18.134-66.665-.007l-22.711-22.71c-18.149-18.129-18.671-48.008.006-66.665l7.698-7.698c-5.337-10.313-9.792-21.046-13.323-32.097h-10.87c-25.988 0-47.133-21.144-47.133-47.133v-32.134c0-25.989 21.145-47.133 47.134-47.133h10.87c3.531-11.05 7.986-21.784 13.323-32.097l-7.704-7.703c-18.666-18.646-18.151-48.528.006-66.665l22.713-22.712c18.159-18.184 48.041-18.638 66.664.006l7.697 7.697c10.313-5.336 21.048-9.792 32.097-13.323v-10.87c0-25.989 21.144-47.133 47.134-47.133h32.133c25.989 0 47.133 21.144 47.133 47.133v10.871c11.049 3.53 21.784 7.986 32.097 13.323l7.704-7.704c18.659-18.682 48.548-18.134 66.665.007l22.711 22.71c18.149 18.129 18.671 48.008-.006 66.665l-7.698 7.698c5.337 10.313 9.792 21.046 13.323 32.097h10.87c25.989 0 47.134 21.144 47.134 47.133v32.134c0 25.989-21.145 47.133-47.134 47.133h-10.87c-3.531 11.05-7.986 21.784-13.323 32.097l7.704 7.704c18.666 18.646 18.151 48.528-.006 66.665l-22.713 22.712c-18.159 18.184-48.041 18.638-66.664-.006l-7.697-7.697c-10.313 5.336-21.048 9.792-32.097 13.323v10.871c0 25.987-21.144 47.131-47.134 47.131zm-106.349-102.83c14.327 8.473 29.747 14.874 45.831 19.025 6.624 1.709 11.252 7.683 11.252 14.524v22.148c0 9.447 7.687 17.133 17.134 17.133h32.133c9.447 0 17.134-7.686 17.134-17.133v-22.148c0-6.841 4.628-12.815 11.252-14.524 16.084-4.151 31.504-10.552 45.831-19.025 5.895-3.486 13.4-2.538 18.243 2.305l15.688 15.689c6.764 6.772 17.626 6.615 24.224.007l22.727-22.726c6.582-6.574 6.802-17.438.006-24.225l-15.695-15.695c-4.842-4.842-5.79-12.348-2.305-18.242 8.473-14.326 14.873-29.746 19.024-45.831 1.71-6.624 7.684-11.251 14.524-11.251h22.147c9.447 0 17.134-7.686 17.134-17.133v-32.134c0-9.447-7.687-17.133-17.134-17.133h-22.147c-6.841 0-12.814-4.628-14.524-11.251-4.151-16.085-10.552-31.505-19.024-45.831-3.485-5.894-2.537-13.4 2.305-18.242l15.689-15.689c6.782-6.774 6.605-17.634.006-24.225l-22.725-22.725c-6.587-6.596-17.451-6.789-24.225-.006l-15.694 15.695c-4.842 4.843-12.35 5.791-18.243 2.305-14.327-8.473-29.747-14.874-45.831-19.025-6.624-1.709-11.252-7.683-11.252-14.524v-22.15c0-9.447-7.687-17.133-17.134-17.133h-32.133c-9.447 0-17.134 7.686-17.134 17.133v22.148c0 6.841-4.628 12.815-11.252 14.524-16.084 4.151-31.504 10.552-45.831 19.025-5.896 3.485-13.401 2.537-18.243-2.305l-15.688-15.689c-6.764-6.772-17.627-6.615-24.224-.007l-22.727 22.726c-6.582 6.574-6.802 17.437-.006 24.225l15.695 15.695c4.842 4.842 5.79 12.348 2.305 18.242-8.473 14.326-14.873 29.746-19.024 45.831-1.71 6.624-7.684 11.251-14.524 11.251h-22.148c-9.447.001-17.134 7.687-17.134 17.134v32.134c0 9.447 7.687 17.133 17.134 17.133h22.147c6.841 0 12.814 4.628 14.524 11.251 4.151 16.085 10.552 31.505 19.024 45.831 3.485 5.894 2.537 13.4-2.305 18.242l-15.689 15.689c-6.782 6.774-6.605 17.634-.006 24.225l22.725 22.725c6.587 6.596 17.451 6.789 24.225.006l15.694-15.695c3.568-3.567 10.991-6.594 18.244-2.304z"/><path d="m256 367.4c-61.427 0-111.4-49.974-111.4-111.4s49.973-111.4 111.4-111.4 111.4 49.974 111.4 111.4-49.973 111.4-111.4 111.4zm0-192.8c-44.885 0-81.4 36.516-81.4 81.4s36.516 81.4 81.4 81.4 81.4-36.516 81.4-81.4-36.515-81.4-81.4-81.4z"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Instrumentación</h3>
											</div>
										</div>
                                    </a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="464pt" viewBox="0 0 464 464" width="464pt" xmlns="http://www.w3.org/2000/svg"><path d="m416 8c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v88h16zm0 0"/><path d="m304 8c0-4.417969-3.582031-8-8-8s-8 3.582031-8 8v88h16zm0 0"/><path d="m336 240v48h32v-48c0-8.835938-7.164062-16-16-16s-16 7.164062-16 16zm0 0"/><path d="m320.046875 239.535156c0-17.644531 14.304687-31.949218 31.953125-31.949218s31.953125 14.304687 31.953125 31.949218c36.433594-4.089844 63.996094-34.871094 64.046875-71.535156v-64c0-4.417969-3.582031-8-8-8h-8v8c0 4.417969-3.582031 8-8 8h-32c-4.417969 0-8-3.582031-8-8v-8h-64v8c0 4.417969-3.582031 8-8 8h-32c-4.417969 0-8-3.582031-8-8v-8h-8c-4.417969 0-8 3.582031-8 8v64c.050781 36.664062 27.613281 67.445312 64.046875 71.535156zm0 0"/><path d="m440 408h-159.121094c-3.890625.128906-7.402344-2.308594-8.640625-6-.746093-2.777344.117188-5.738281 2.242188-7.679688 1.433593-1.515624 3.433593-2.355468 5.519531-2.320312h56c13.238281-.039062 23.960938-10.761719 24-24v-64h-16v64c0 4.417969-3.582031 8-8 8h-56c-7.402344.003906-14.390625 3.425781-18.929688 9.277344-4.535156 5.851562-6.113281 13.472656-4.269531 20.644531 2.914063 10.84375 12.851563 18.304687 24.078125 18.078125h159.121094c4.417969 0 8 3.582031 8 8v8c0 4.417969-3.582031 8-8 8h-344v-208h-16v216c0 4.417969 3.582031 8 8 8h352c13.238281-.039062 23.960938-10.761719 24-24v-8c-.039062-13.238281-10.761719-23.960938-24-24zm0 0"/><path d="m88.609375 91.0625c-1.238281-2.988281-.554687-6.429688 1.734375-8.71875l26.34375-26.34375h-61.597656l-10.722656 24h13c2.640624 0 5.109374 1.304688 6.601562 3.480469 1.492188 2.179687 1.8125 4.953125.855469 7.414062l-5.128907 13.105469h20.304688c2.625-.003906 5.085938 1.285156 6.582031 3.441406 1.496094 2.160156 1.839844 4.917969.914063 7.375l-11.039063 29.40625 48.230469-48.222656h-28.6875c-3.234375 0-6.152344-1.949219-7.390625-4.9375zm0 0"/><path d="m176 0h-176v224h176zm-26.34375 93.65625-88 88c-2.664062 2.671875-6.828125 3.121094-10 1.078125s-4.484375-6.023437-3.152344-9.550781l19.953125-53.183594h-20.457031c-2.640625 0-5.109375-1.304688-6.601562-3.480469-1.488282-2.179687-1.808594-4.953125-.855469-7.414062l5.097656-13.105469h-13.640625c-2.710938 0-5.238281-1.371094-6.710938-3.648438-1.476562-2.273437-1.699218-5.140624-.59375-7.617187l17.863282-40c1.289062-2.878906 4.148437-4.734375 7.304687-4.734375h86.136719c3.234375 0 6.152344 1.949219 7.390625 4.9375s.550781 6.429688-1.734375 8.71875l-26.34375 26.34375h28.6875c3.234375 0 6.152344 1.949219 7.390625 4.9375s.550781 6.429688-1.734375 8.71875zm0 0"/></svg>									
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Eléctrica</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="496pt" viewBox="0 0 496 496" width="496pt" xmlns="http://www.w3.org/2000/svg"><path d="m496 208h-17.976562l-26.792969 14.097656c-3.015625-1.664062-6.152344-3.074218-9.535157-3.921875l-33.703124-8.433593v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-31.078125-24.351562-57.085937-54.28125-57.992187-1.519531-.039063-3.015625.015624-4.519531.089843l8.738281-20.457031-42.363281-42.351562-24.972657 16.253906c-9.488281-5.34375-19.523437-9.503906-29.945312-12.421875l-6.167969-29.136719h-60.96875l-6.167969 29.144531c-10.421874 2.917969-20.464843 7.078125-29.945312 12.421875l-24.972656-16.253906-42.328125 42.328125 8.589843 20.472656c-.328124-.015625-.640624-.074219-.96875-.082031-15.320312-.445312-29.75 5.144531-40.710937 15.785156-10.976563 10.632813-17.015625 24.902344-17.015625 40.183594v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-21.984375 5.496093-39.847656-23.238281h-26.167969v165.769531l78.703125 26.230469h15.328125l1.914062-6.703125 24.125 14.480469c.394532 13.117187 10.914063 23.671875 24.019532 24.132812.460937 13.050782 10.949218 23.539063 24 24 .460937 13.050782 10.949218 23.539063 24 24 .476562 13.355469 11.414062 24.089844 24.878906 24.089844 5 0 9.902344-1.558594 14.023438-4.335938l7.085937 6.257813c4.441406 3.910156 10.160156 6.078125 16.097656 6.078125h1.480469c13.3125 0 24.144531-10.761719 24.3125-24.03125 13.167969-.167969 23.832031-10.832031 24-24 13.175781-.167969 23.847656-10.847656 24-24.023438 13.296875-.3125 24.03125-11.175781 24.03125-24.546874 0-.804688-.039062-1.597657-.121094-2.398438l144.121094-72.054688zm-80-61.984375v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 39.96875 9.984375-87.976562 46.304687v-21.550781c0-10.753906-4.34375-20.601563-11.457031-27.878906l27.457031-6.859375v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.222656-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187zm-155.9375 126.230469-36.214844-11.574219c-16.503906-5.289063-34.367187-6.078125-51.261718-2.335937l-4.585938 1.015624v-2.367187c0-11.03125 7.480469-20.609375 18.175781-23.289063l45.824219-11.449218v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.289062-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 45.816407 11.449218c10.695312 2.679688 18.175781 12.257813 18.175781 23.289063v29.976563l-1.976562 1.039062h-4.902344l-49.183594-14.054688c-3.875-1.097656-7.867188-1.546874-11.875-1.699218zm-90.269531-152.511719c15.023437-29.109375 45.261719-47.734375 78.207031-47.734375 32.632812 0 62.742188 18.375 77.878906 47.089844-3.808594 7.636718-5.878906 16.09375-5.878906 24.910156v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-32 8.011718-32.007812-8.011718v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-31.078125-24.351562-57.085937-54.28125-57.992187-15.3125-.449219-29.75 5.144531-40.710938 15.785156-10.96875 10.640625-17.007812 24.910156-17.007812 40.191406v16c0 18.3125 9.089844 35.496094 24 45.902344v3.839844l-32 8.011718-32.007812-8.011718v-3.832032c14.910156-10.40625 24.007812-27.597656 24.007812-45.910156v-13.984375c0-9.414063-2.265625-18.351563-6.207031-26.28125zm-36.792969-48.382813 25.558594-25.558593 22.496094 14.640625 4.304687-2.640625c10.785156-6.609375 22.441406-11.449219 34.640625-14.367188l4.921875-1.179687 5.566406-26.246094h35.03125l5.550781 26.238281 4.921876 1.175781c12.199218 2.921876 23.855468 7.761719 34.640624 14.371094l4.304688 2.640625 22.496094-14.640625 25.527344 25.535156-9.074219 21.246094c-6.175781 2.65625-11.902344 6.394532-16.886719 11.226563-.328125.320312-.609375.664062-.929688.992187-18.847656-29.960937-52.167968-48.785156-88.070312-48.785156-36.160156 0-69.574219 19-88.375 49.265625-5.03125-5.265625-11.015625-9.53125-17.648438-12.511719zm-29 150.894532v-25.335938l-3.992188-2.308594c-12.34375-7.136718-20.007812-20.402343-20.007812-34.601562v-16c0-10.910156 4.3125-21.113281 12.152344-28.710938 7.832031-7.59375 18.246094-11.6875 29.085937-11.265624 21.371094.640624 38.761719 19.480468 38.761719 41.992187v13.984375c0 14.199219-7.671875 27.464844-20.023438 34.617188l-4 2.3125.03125 25.316406 27.449219 6.859375c-7.113281 7.277343-11.457031 17.125-11.457031 27.878906v5.925781l-24.953125 5.546875.648437-2.265625.304688-14.785156-42.191406-24.613281zm-88-14.246094h5.832031l57.535157 33.558594-29.03125 116.121094-34.335938-11.449219zm65.296875 160-15.746094-5.246094 28.203125-112.800781 18.246094 10.640625v2.285156l-30.03125 105.121094zm54.703125 23.03125c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm24 24c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm24 24c0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625-4.945312 0-8.96875-4.023438-8.96875-8.96875zm32.96875 32.96875c-4.945312 0-8.96875-4.023438-8.96875-8.96875 0-2.359375.953125-4.671875 2.625-6.34375l14.0625-14.0625c1.671875-1.671875 3.984375-2.625 6.34375-2.625 4.945312 0 8.96875 4.023438 8.96875 8.96875 0 2.359375-.953125 4.671875-2.625 6.34375l-14.0625 14.0625c-1.671875 1.671875-3.984375 2.625-6.34375 2.625zm38.6875 8h-1.480469c-2.039062 0-3.992187-.742188-5.511719-2.089844l-5.984374-5.277344 6.007812-6.007812c2.144531-2.144531 3.808594-4.695312 5.039062-7.433594l7.273438 6.054688c1.902344 1.585937 3 3.921875 3 6.410156 0 4.601562-3.742188 8.34375-8.34375 8.34375zm71.742188-72h-1.382813c-2.007813 0-3.96875-.710938-5.503906-2l-43.382813-36.152344-10.25 12.296875 42.128906 35.101563c1.894532 1.585937 2.992188 3.921875 2.992188 6.410156 0 4.601562-3.742188 8.34375-8.34375 8.34375h-1.746094c-1.941406 0-3.839844-.6875-5.335937-1.9375l-43.453125-36.207031-10.25 12.296875 42.128906 35.105468c1.902344 1.574219 3 3.910157 3 6.398438 0 4.601562-3.742188 8.34375-8.34375 8.34375-3.070312 0-6.0625-1.089844-8.417969-3.046875l-17.855469-14.882813c-3.933593-8.039062-12.015624-13.644531-21.480468-13.980468-.460938-13.050782-10.949219-23.539063-24-24-.460938-13.050782-10.949219-23.539063-24-24-.46875-13.355469-11.40625-24.089844-24.871094-24.089844-6.574219 0-13.007812 2.664062-17.65625 7.3125l-14.0625 14.0625c-.839844.839844-1.535156 1.792969-2.242188 2.738281l-24.574218-14.746093 21.542968-75.398438 54.007813-12.007812c14.167969-3.167969 29.105469-2.480469 42.921875 1.941406l11.785156 3.769531-39.953125 19.976563c-9.128906 4.574218-14.800781 13.757812-14.800781 23.96875v1.582031c0 14.777343 12.023438 26.800781 26.800781 26.800781 4.855469 0 9.632813-1.320312 13.785157-3.824219l29.789062-17.863281c5.984375-3.585938 14.015625-2.800781 19.183594 1.847656l67.59375 60.832032c1.808594 1.640624 2.847656 3.96875 2.847656 6.40625 0 4.746093-3.855469 8.601562-8.601562 8.601562zm152.601562-92.945312-134.777344 67.394531c-.4375-.464844-.894531-.90625-1.375-1.335938l-67.582031-60.832031c-5.921875-5.34375-13.578125-8.28125-21.554687-8.28125-5.832032 0-11.558594 1.585938-16.574219 4.59375l-29.800781 17.871094c-1.671876 1.007812-3.582032 1.535156-5.535157 1.535156-5.960937 0-10.800781-4.839844-10.800781-10.800781v-1.582031c0-4.121094 2.289062-7.816407 5.96875-9.65625l44.625-22.3125c7.636719-3.824219 16.703125-4.679688 24.933594-2.3125l50.273437 14.359374 12.175781.304688 150.023438-78.960938zm0 0"/></svg>											
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Flexibilidad</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="496pt" viewBox="0 0 496 496" width="496pt" xmlns="http://www.w3.org/2000/svg">
													<g>
														<g>
															<path d="M339.103,220.58c-4.583,0-9.953-1.748-13.443-5.245l-10.209-8.634v120.471
																c0,0.227-0.03,0.43-0.03,0.656c34.632-33.068,56.166-80.523,56.166-136.85c0-102.606-83.184-185.79-185.796-185.79
																S0,88.372,0,190.979c0,53.559,20.192,99.76,52.908,132.9V203.664l-10.096,11.671c-0.871,0.871-0.239,1.641-1.229,2.297
																c-2.977,1.969-6.414,2.948-9.851,2.948s-6.874-0.985-9.851-2.948c-0.99-0.656-1.939-1.42-2.81-2.297
																c-6.993-6.987-6.999-18.324-0.006-25.311L160.473,48.603c0.877-0.877,1.85-1.533,2.792-2.297
																c14.046-11.444,34.679-10.782,47.765,2.297L352.356,190.03c6.993,6.987,6.796,18.324-0.197,25.311
																C348.662,218.832,343.686,220.58,339.103,220.58z"/>
															<path d="M88.709,167.863v171.249c0,7.357,3.855,13.604,9.708,18.271c5.776,2.989,11.737,5.663,17.853,8.043
																c3.073,0.621,6.212,0.973,9.326,0.973h126.014c0.788,0,1.486-0.131,2.244-0.173c6.629-2.47,13.085-5.281,19.321-8.461
																c4.111-4.636,6.468-10.919,6.468-18.652V170.906l-96.985-96.991L88.709,167.863z M236.008,329.601H132.351V225.944h103.656
																V329.601z M226.52,181.557v35.682H208.62v-35.682c0-3.497-2.846-6.343-6.337-6.343h-37.388c-3.497,0-6.343,2.852-6.343,6.343
																v35.682h-17.901v-35.682c0-13.366,10.878-24.243,24.243-24.243h37.388C215.649,157.314,226.52,168.191,226.52,181.557z"/>
														</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
													</svg>									
												<h6 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Seguridad Industrial</h6>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
							                    <svg height="512pt" viewBox="-24 0 512 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m154.644531 167.144531h-106.089843c-.007813 3.78125 0 320.902344 0 320.902344 0 13.226563 10.652343 23.953125 23.882812 23.953125s23.953125-10.726562 23.953125-23.953125v-191.480469h10.34375v191.480469c0 13.226563 10.726563 23.953125 23.957031 23.953125 13.230469 0 23.953125-10.726562 23.953125-23.953125 0 0 .003907-320.800781 0-320.902344zm0 0"/><path d="m464.0625 273.265625v-42.027344c0-5.898437-4.777344-10.675781-10.675781-10.675781h-30.222657c-3.300781-13.570312-8.667968-26.339844-15.761718-37.953125l21.402344-21.398437c4.167968-4.167969 4.167968-10.929688 0-15.097657l-29.71875-29.71875c-4.171876-4.171875-10.929688-4.171875-15.097657 0l-21.402343 21.398438c-11.609376-7.089844-24.378907-12.457031-37.949219-15.761719v-30.21875c0-5.898438-4.78125-10.675781-10.675781-10.675781h-42.03125c-5.894532 0-10.675782 4.777343-10.675782 10.675781v30.222656c-13.566406 3.300782-26.339844 8.667969-37.949218 15.757813l-10.96875-10.96875c2.183593 4.730469 3.789062 9.769531 4.71875 15.039062.589843 3.359375.921874 6.808594.941406 10.328125v.171875 73.429688c11.203125-31.644531 41.898437-54.15625 77.589844-52.992188 40.972656 1.332031 74.550781 34.171875 76.738281 75.105469 2.453125 45.796875-34.105469 83.839844-79.378907 83.839844-34.558593 0-64.023437-22.175782-74.949218-53.035156v20.3125c0 19.316406-15.683594 34.832031-34.832032 34.832031-4.484374 0-8.765624-.859375-12.699218-2.402344v40.3125l16.34375 16.34375c4.167968 4.167969 10.925781 4.167969 15.097656 0l21.398438-21.398437c11.609374 7.089843 24.382812 12.457031 37.949218 15.757812v30.226562c0 5.894532 4.78125 10.675782 10.675782 10.675782h42.03125c5.894531 0 10.675781-4.78125 10.675781-10.675782v-30.226562c13.570312-3.300781 26.339843-8.667969 37.949219-15.757812l21.402343 21.398437c4.167969 4.167969 10.925781 4.167969 15.097657 0l29.71875-29.71875c4.167968-4.171875 4.167968-10.929687 0-15.097656l-21.402344-21.402344c7.09375-11.609375 12.460937-24.378906 15.761718-37.949219h30.222657c5.898437 0 10.675781-4.777344 10.675781-10.675781zm0 0"/><path d="m48.695312 152h15.765626c0-10.453125 0-37.089844 0-46.742188h-16.480469c-26.324219 0-47.851563 21.085938-47.980469 47.105469v146.660157c0 11.023437 8.9375 19.960937 19.960938 19.960937 11.027343 0 19.964843-8.9375 19.964843-19.960937v-146.558594c.019531-3.808594 3.855469-7.28125 8.054688-7.28125h.714843zm0 0"/><path d="m203.128906 299.023438v-146.757813c-.132812-25.917969-21.65625-47.007813-47.980468-47.007813h-16.480469v46.742188h15.765625v-6.816406h.714844c4.195312 0 8.035156 3.472656 8.054687 7.179687v146.660157c0 11.023437 8.9375 19.960937 19.960937 19.960937 11.027344 0 19.964844-8.9375 19.964844-19.960937zm0 0"/><path d="m79.605469 105.257812v46.742188h43.917969c0-10.453125 0-37.089844 0-46.742188-15.703126 0-28.171876 0-43.917969 0zm0 0"/><path d="m141.597656 61.164062h-80.066406c4.734375 18.175782 21.230469 30.910157 40.03125 30.910157 18.835938-.003907 35.308594-12.769531 40.035156-30.910157zm0 0"/><path d="m60.226562 50.285156h82.332032c5.707031 0 10.597656-4.464844 10.703125-10.167968.105469-5.8125-4.574219-10.554688-10.359375-10.554688h-1.695313c-1.15625-3.898438-2.878906-7.554688-5.0625-10.878906-3.214843-4.890625-7.433593-9.058594-12.367187-12.207032v8.75c0 4.179688-3.390625 7.570313-7.574219 7.570313-4.179687 0-7.570313-3.390625-7.570313-7.570313v-14.613281c-2.296874-.394531-4.65625-.613281-7.070312-.613281-2.410156 0-4.769531.21875-7.066406.613281v14.609375c0 4.183594-3.390625 7.574219-7.570313 7.574219-4.183593 0-7.574219-3.390625-7.574219-7.574219v-8.746094c-4.933593 3.148438-9.152343 7.316407-12.367187 12.203126-2.183594 3.324218-3.90625 6.980468-5.0625 10.878906h-1.351563c-5.703124 0-10.597656 4.464844-10.703124 10.167968-.105469 5.8125 4.574218 10.558594 10.359374 10.558594zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Civil Estructural</h3>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="row mb-2" style="justify-content:center">
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
                                                <svg id="Layer_1" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m54 35c0-1.7-1.8-3-4-3s-4 1.3-4 3v3l-4.9-2.1c1.2-1.1 2-2.5 2.5-4l2.7-1.4c1-.5 1.7-1.5 1.7-2.7v-1.8c0-1.7-1.3-3-3-3h-.4l-.6.6v-2.6h2v-3c0-6.2-4.1-11.7-10-13.4-.2-1.5-1.5-2.6-3-2.6h-2c-1.5 0-2.8 1.1-3 2.6-5.9 1.7-10 7.2-10 13.4v3h2v2.6l-.6-.6h-.4c-1.7 0-3 1.3-3 3v1.8c0 1.1.6 2.2 1.7 2.7l2.7 1.4c.5 1.5 1.3 2.9 2.5 4l-8.3 3.6c-4 1.8-6.6 5.7-6.6 10.1v12.4h50v-29.1l-4 5.6zm-6 11.8v-9.2c1.3.5 2.7.5 4 0v3.6zm2-12.8c1.2 0 2 .6 2 1s-.8 1-2 1-2-.6-2-1 .8-1 2-1zm-4 8.7-6.5 4.8c-.5.3-1.2.5-1.8.5h-3.4l.3-1.2c.1-.4.5-.8 1-.8h1.4c1.6 0 3.1-.8 4.1-2.1l3.2-4.5 1.8.8v2.5zm-13.3 3.6-.7 2.6-.7-2.6c-.3-1.4-1.5-2.3-2.9-2.3h-1.4c-1 0-1.9-.5-2.4-1.3l-3-4.2 3.1-1.4 2.9 1.6c2.7 1.5 5.9 1.5 8.6 0l2.9-1.6 3.1 1.4-3 4.2c-.6.8-1.5 1.3-2.4 1.3h-1.4c-1.2 0-2.4.9-2.7 2.3zm-3 1.7h-3.4c-.6 0-1.3-.2-1.8-.6l-8.7-6.3 3.9-1.7 3.2 4.5c.9 1.3 2.5 2.1 4.1 2.1h1.4c.5 0 .9.3 1 .8zm15.7-22.9c.4.1.6.5.6.9v1.8c0 .4-.2.7-.6.9l-1.4.7v-.1-2.8zm-25.2-6.1c.4-1.2 1.5-2 2.8-2h18c1.3 0 2.4.8 2.8 2zm7.8-12.3v6.3h2v-8c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v8h2v-6.3c4.1 1.4 7.1 4.9 7.8 9.2-.8-.6-1.8-.9-2.8-.9h-18c-1 0-2 .3-2.8.9.7-4.3 3.7-7.8 7.8-9.2zm-9.4 22c-.3-.2-.6-.5-.6-.9v-1.8c0-.4.3-.8.6-.9l1.4 1.4v2.8.1zm3.4.6v-8.3h20v8.3c0 2.6-1.4 4.9-3.6 6.1l-3 1.6c-2.1 1.1-4.6 1.1-6.7 0l-3-1.6c-2.3-1.2-3.7-3.6-3.7-6.1zm-12 20.2c0-3 1.5-5.7 3.9-7.4l9.5 6.9c.9.6 1.9 1 2.9 1h4.7v10h-3v-8h-7.2l-.8-3.2-2 .4.7 2.8h-1.7v8h-7zm16 4.5v2h-7v-2zm-7 4h7v2h-7zm14 2v-10h4.7c1.1 0 2.1-.3 2.9-1l5.4-3.9v4.4l-7.5 10.5zm23-20.9v20.9h-15l13-18v-.1z"/><path d="m54 58v-13.3l-8.9 13.3zm-2-2h-3.1l3.1-4.7z"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Civil Concreto</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="512pt" viewBox="0 0 512 512" width="512pt" xmlns="http://www.w3.org/2000/svg"><path d="m497 201.992188h-53.648438l-27.816406-35.871094c.609375.007812 1.21875.03125 1.828125.03125 23.308594 0 45.703125-8.261719 63.410157-23.460938 3.167968-2.722656 5.058593-6.640625 5.214843-10.8125.160157-4.171875-1.429687-8.222656-4.382812-11.175781l-27.8125-27.8125 27.902343-65.113281c2.414063-5.628906 1.160157-12.160156-3.164062-16.496094-4.324219-4.339844-10.851562-5.613281-16.488281-3.21875l-65.320313 27.753906-31.421875-31.421875c-2.953125-2.953125-7.003906-4.546875-11.179687-4.3828122-4.171875.1601562-8.089844 2.0468752-10.808594 5.2187502-15.96875 18.597656-24.277344 42.371093-23.398438 66.941406.6875 19.285156 7.027344 37.660156 18.085938 53.121094v76.699219h-81c-8.285156 0-15 6.714843-15 15v83h-91v-118.578126c17.460938-6.195312 30-22.867187 30-42.421874 0-24.8125-20.1875-45-45-45s-45 20.1875-45 45c0 19.554687 12.539062 36.226562 30 42.421874v118.578126h-106c-8.285156 0-15 6.714843-15 15v182c0 8.285156 6.714844 15 15 15h482c8.285156 0 15-6.714844 15-15v-280c0-8.285157-6.714844-15-15-15zm-361-78c8.269531 0 15 6.726562 15 15 0 8.269531-6.730469 15-15 15s-15-6.730469-15-15c0-8.273438 6.730469-15 15-15zm303.417969-73.71875-8.472657 19.769531-11.347656-11.347657zm-82.445313-11.78125 90.535156 90.539062c-25.359374 12.613281-57.070312 8.175781-77.894531-12.644531-20.820312-20.824219-25.257812-52.53125-12.640625-77.894531zm11.027344 115.273437c.078125.109375.160156.214844.246094.324219l37.144531 47.902344h-37.390625zm-338 176.226563h212v152h-212zm316 152v-72h62v72zm136 0h-44v-87c0-8.285157-6.714844-15-15-15h-92c-8.285156 0-15 6.714843-15 15v87h-44v-250h210zm0 0"/><path d="m301.996094 276.992188c0 8.285156 6.714844 15 15 15h120.003906c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15h-120.003906c-8.285156 0-15 6.714843-15 15zm0 0"/><path d="m437 321.992188h-120.003906c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h120.003906c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"/><path d="m105 359.992188h-30c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h30c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"/><path d="m195 359.992188h-30c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h30c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"/><path d="m105 419.992188h-30c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h30c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"/><path d="m195 419.992188h-30c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h30c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"/><path d="m54.933594 193.066406c2.183594 0 4.402344-.476562 6.5-1.488281 7.464844-3.597656 10.597656-12.5625 7.003906-20.023437-4.867188-10.097657-7.4375-21.355469-7.4375-32.5625 0-9.28125 1.695312-18.375 5.039062-27.03125 2.984376-7.726563-.859374-16.410157-8.585937-19.398438-7.726563-2.984375-16.410156.859375-19.394531 8.585938-4.683594 12.121093-7.058594 24.851562-7.058594 37.84375 0 15.6875 3.601562 31.449218 10.410156 45.582031 2.585938 5.363281 7.941406 8.492187 13.523438 8.492187zm0 0"/><path d="m210.066406 191.578125c2.097656 1.007813 4.316406 1.488281 6.5 1.488281 5.582032 0 10.9375-3.128906 13.523438-8.492187 6.808594-14.132813 10.410156-29.894531 10.410156-45.582031 0-12.992188-2.375-25.722657-7.058594-37.84375-2.984375-7.726563-11.671875-11.570313-19.394531-8.585938-7.730469 2.988281-11.574219 11.671875-8.585937 19.398438 3.34375 8.660156 5.039062 17.75 5.039062 27.03125 0 11.207031-2.570312 22.464843-7.4375 32.558593-3.59375 7.464844-.460938 16.429688 7.003906 20.027344zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Telecomunicación</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="511pt" viewBox="0 -17 511.99979 511" width="511pt" xmlns="http://www.w3.org/2000/svg"><path d="m506.078125 308.792969-2.558594-7.90625-48.820312-12.226563c.300781-4.429687.4375-8.875.4375-13.363281 0-56.921875-24.449219-111.230469-67.074219-149.003906l-19.890625 22.441406c36.214844 32.085937 56.976563 78.21875 56.976563 126.5625 0 1.988281-.027344 3.96875-.109376 5.9375l-31.699218-7.9375c-.449219-31.117187-11.523438-61.335937-31.324219-85.328125l-23.132813 19.09375c13.753907 16.652344 22.121094 37.183594 24.0625 58.625l-32.308593-8.085938c-3.488281-14.734374-11.257813-27.839843-21.914063-37.925781l49.121094-171.363281-5.554688-6.167969c-23.8125-26.460937-58.878906-41.644531-96.195312-41.644531s-72.371094 15.183594-96.183594 41.644531l-5.558594 6.167969 13.847657 48.320312c-10.867188 5.367188-21.304688 11.765626-31.199219 19.171876-33.4375 25-58.648438 60.628906-70.992188 100.304687l28.636719 8.90625c13.625-43.824219 43.542969-78.78125 81.941407-99.125l9.054687 31.609375c-11.796875 7.058594-22.589844 15.902344-31.925781 26.351562l22.359375 19.980469c5.476562-6.128906 11.597656-11.554687 18.203125-16.195312l9.1875 32.050781c-10.597656 10.035156-18.34375 23.058594-21.851563 37.703125l-32.339843 8.007813c1.109374-12.382813 4.339843-24.320313 9.648437-35.617188l-27.152344-12.742188c-8.253906 17.589844-12.601562 36.382813-12.921875 55.886719l-110.238281 27.289063-2.582031 7.898437c-11.074219 33.835938-6.753906 71.792969 11.847656 104.148438 18.601562 32.347656 49.230469 55.179687 84.039062 62.636719l8.125 1.738281 35.039063-36.132813c32.636719 21.949219 71.230469 33.824219 111.015625 33.824219 13.734375 0 27.449219-1.398437 40.773438-4.175781l-6.117188-29.359375c-11.316406 2.359375-22.96875 3.546875-34.65625 3.546875-32.6875 0-63.527344-9.433594-89.675781-25.859375l22.691406-23.402344c19.824219 11.136719 42.675781 17.484375 66.984375 17.484375v-29.988281c-16.132812 0-31.4375-3.578125-45.171875-9.996094l23.171875-23.910156c6.964844 2.097656 14.351562 3.226562 22 3.226562 7.578125 0 14.894531-1.097656 21.8125-3.167969l123.703125 128.300782 8.125-1.71875c34.828125-7.355469 65.527344-30.109375 84.210937-62.40625 18.691407-32.296875 23.109376-70.242188 12.132813-104.109375zm-200.761719 25.148437c13.464844-11.3125 23-27.167968 26.207032-45.203125l148.175781 37.097657c5.40625 23.75 1.289062 49.582031-11.707031 72.050781-13.003907 22.464843-33.335938 38.90625-56.628907 46.054687zm-117.445312-277.5c17.851562-16.574218 42.273437-25.953125 68.222656-25.953125 25.960938 0 50.382812 9.378907 68.222656 25.953125l-42.082031 146.84375c-8.15625-2.980468-16.964844-4.601562-26.140625-4.601562s-17.984375 1.621094-26.140625 4.601562zm-87.496094 387.078125c-23.273438-7.207031-43.566406-23.710937-56.5-46.210937s-16.992188-48.339844-11.515625-72.074219l148.285156-36.703125c3.15625 18.050781 12.644531 33.9375 26.089844 45.28125zm109.105469-168.222656c0-25.710937 20.914062-46.625 46.613281-46.625 25.710938 0 46.621094 20.914063 46.621094 46.625 0 25.699219-20.910156 46.609375-46.621094 46.609375-25.699219.003906-46.613281-20.910156-46.613281-46.609375zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> HVAC</h3>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<br><br>
						<div class="featured-boxes featured-boxes-style-4 custom-featured-boxes-style-1">
							<h1 style="text-transform: uppercase; color: #9a111e;" class="d-block custom-font-secondary font-weight-semibold appear-animation text-center" data-appear-animation="maskUp">Procura de Materiales</h1>
							<div class="row mb-2" style="justify-content:center">
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg id="Layer_5" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512" xmlns="http://www.w3.org/2000/svg"><path d="m49.726 20.547 11.099-3.171c1.28-.366 2.175-1.553 2.175-2.885v-5.491c0-1.654-1.346-3-3-3h-3v-2c0-1.654-1.346-3-3-3h-18c-1.654 0-3 1.346-3 3v6c0 1.654 1.346 3 3 3h18c1.654 0 3-1.346 3-3v-2h3c.551 0 1 .449 1 1v5.491c0 .444-.298.84-.726.962l-11.099 3.171c-1.28.366-2.175 1.553-2.175 2.885v3.491h-1c-1.103 0-2 .897-2 2v8h-25v-20.184c1.161-.414 2-1.514 2-2.816v-1.81c0-.656.534-1.19 1.19-1.19.284 0 .559.102.774.287l8.036 6.887v-4.174c0-6.065-4.935-11-11-11h-8.764c-1.143 0-2.171.635-2.683 1.658l-.171.342h-.764l-.171-.342c-.511-1.023-1.54-1.658-2.683-1.658h-.764c-1.654 0-3 1.346-3 3v6c0 1.654 1.346 3 3 3h.764c1.143 0 2.171-.635 2.683-1.658l.171-.342h1.382v1c0 1.302.839 2.402 2 2.816v20.184h-10v14h4v14h8v-4h38v4h8v-14h4v-14h-11v-8c0-1.103-.897-2-2-2h-1v-3.491c0-.445.298-.84.726-.962zm5.274-10.547c0 .551-.449 1-1 1h-18c-.551 0-1-.449-1-1v-6c0-.551.449-1 1-1h18c.551 0 1 .449 1 1zm-5.414 27 10 10h-5.172l-10-10zm-28 0 10 10h-9.172l-10-10zm20.828 10-10-10h9.172l10 10zm-2.828 0h-5.172l-10-10h5.172zm-20 0h-5.172l-10-10h5.172zm-6.586 2h38v4h-38zm-6.618-40-.724 1.447c-.17.341-.513.553-.894.553h-.764c-.551 0-1-.449-1-1v-6c0-.551.449-1 1-1h.764c.381 0 .724.212.894.553l.724 1.447h3.236l.724-1.447c.17-.341.513-.553.894-.553h8.764c4.904 0 8.904 3.942 8.999 8.824l-4.732-4.056c-.579-.495-1.316-.768-2.077-.768-1.759 0-3.19 1.431-3.19 3.19v1.81c0 .551-.449 1-1 1h-6c-.551 0-1-.449-1-1v-3zm6.618 6h4v20h-4zm-10 23.414 8.586 8.586h-8.586zm4 22.586v-12h4v12zm6-4v-2h38v2zm44 4h-4v-12h4zm4-15.414-8.586-8.586h8.586zm-11-10.586h-4v-8h4z"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Ingeniería de materiales</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg id="Layer_5" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512" xmlns="http://www.w3.org/2000/svg"><g><path d="m25 31h2v2h-2z"/><path d="m21 31h2v2h-2z"/><path d="m25 35h2v2h-2z"/><path d="m21 35h2v2h-2z"/><path d="m25 39h2v2h-2z"/><path d="m21 39h2v2h-2z"/><path d="m36.736 19.186 1.965-.371c-.13-.687-.305-1.376-.522-2.047l-1.902.614c.19.592.345 1.199.459 1.804z"/><path d="m35.136 11.208c-.449-.546-.937-1.069-1.448-1.553l-1.375 1.452c.451.427.882.889 1.278 1.371z"/><path d="m35.59 15.64 1.811-.85c-.3-.64-.644-1.266-1.021-1.862l-1.689 1.069c.331.526.634 1.079.899 1.643z"/><path d="m7 22h-2c0 4.716 1.887 9.101 5.313 12.345l1.375-1.452c-3.023-2.863-4.688-6.731-4.688-10.893z"/><path d="m59 25h-8c-1.103 0-2-.897-2-2s.897-2 2-2h8c2.206 0 4-1.794 4-4s-1.794-4-4-4h-18.036c-1.095-2.302-2.597-4.419-4.513-6.237-4.22-4.003-9.733-6.036-15.585-5.733-10.903.574-19.627 9.583-19.861 20.51-.13 6.099 2.396 11.942 6.932 16.033 1.645 1.483 2.67 3.382 2.963 5.427h-.9c-1.654 0-3 1.346-3 3 0 .771.301 1.468.78 2-.479.532-.78 1.229-.78 2 0 1.654 1.346 3 3 3h1.051c.507 5.598 5.221 10 10.949 10 2.979 0 5.68-1.194 7.663-3.124l1.402 1.402 2.489-1.48c.317.147.638.281.956.4l.713 2.802h7.555l.713-2.802c.318-.119.639-.252.956-.4l2.489 1.48 5.342-5.342-1.479-2.489c.147-.317.281-.637.4-.956l2.801-.714v-2.777h5c2.206 0 4-1.794 4-4v-16c0-2.206-1.794-4-4-4zm-49.724 11.088c-4.104-3.701-6.39-8.988-6.271-14.505.212-9.886 8.104-18.037 17.967-18.556 5.279-.273 10.285 1.564 14.104 5.188 3.82 3.624 5.924 8.52 5.924 13.785 0 5.362-2.292 10.501-6.289 14.1-2.002 1.802-3.282 4.276-3.609 6.9h-.102v-15.333l-3.5-4.667h-.5v-10h-4v-4h-2v-4h-2v4h-2v4h-4v30h-.084c-.301-2.617-1.567-5.043-3.64-6.912zm27.724 13.912c0-.771-.301-1.468-.78-2 .48-.532.78-1.229.78-2 0-1.618-1.29-2.932-2.894-2.989 1.294-1.267 3.048-2.011 4.894-2.011 3.859 0 7 3.14 7 7s-3.141 7-7 7c-1.846 0-3.6-.744-4.894-2.011 1.604-.057 2.894-1.371 2.894-2.989zm-16-39v2h-2v-2zm4 4v8h-8v20h-2v-28zm1.5 10 1.5 2h-9v-2zm-7.5 4h10v14h-10zm-9 16h24c.552 0 1 .449 1 1s-.448 1-1 1h-24c-.552 0-1-.449-1-1s.448-1 1-1zm-1 5c0-.551.448-1 1-1h24c.552 0 1 .449 1 1s-.448 1-1 1h-24c-.552 0-1-.449-1-1zm13 11c-4.625 0-8.442-3.507-8.941-8h17.882c-.499 4.493-4.316 8-8.941 8zm30-10.777-2.354.599-.168.528c-.179.559-.416 1.125-.704 1.683l-.256.493 1.246 2.096-3.143 3.143-2.095-1.246-.492.254c-.56.289-1.126.525-1.686.704l-.528.169-.597 2.354h-4.445l-.598-2.354-.528-.169c-.56-.179-1.126-.415-1.686-.704l-.492-.254-2.095 1.246-.413-.413c.826-1.162 1.43-2.488 1.755-3.922 1.661 1.619 3.91 2.57 6.279 2.57 4.963 0 9-4.038 9-9s-4.037-9-9-9c-1.888 0-3.694.614-5.194 1.679.52-1.162 1.276-2.224 2.243-3.093.885-.797 1.691-1.664 2.419-2.586h2.755l.598 2.354.528.169c.56.179 1.126.415 1.686.704l.492.254 2.095-1.246 3.143 3.143-1.246 2.095.255.493c.289.559.526 1.125.705 1.683l.168.528 2.353.6zm9-5.223c0 1.103-.897 2-2 2h-5v-2.777l-2.802-.713c-.119-.319-.253-.639-.4-.957l1.479-2.489-5.342-5.342-2.489 1.48c-.317-.147-.638-.281-.956-.4l-.713-2.802h-2.912c2.024-3.28 3.135-7.084 3.135-11 0-2.432-.429-4.783-1.21-7h17.21c1.103 0 2 .897 2 2s-.897 2-2 2h-8c-2.206 0-4 1.794-4 4s1.794 4 4 4h8c1.103 0 2 .897 2 2z"/></g></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Abastecimiento </h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="496pt" viewBox="0 0 496 496" width="496pt" xmlns="http://www.w3.org/2000/svg"><path d="m496 213.910156v-26.167968c0-19.773438-14.769531-36.796876-34.335938-39.589844l-21.664062-3.089844v-11.175781c14.488281-10.132813 24-26.910157 24-45.886719v-8h16v-16h-16v-8c0-22.054688-17.945312-40-40-40h-32c-22.054688 0-40 17.945312-40 40v8h-16v16h16v8c0 15.664062 6.480469 29.824219 16.886719 40h-48.886719v-104c0-13.230469-10.769531-24-24-24h-160c-13.230469 0-24 10.769531-24 24v152h-112v48h16v211.054688l-9.886719 4.945312 80 40h273.144531l1.429688 16h94.609375l16.703125-184v-.097656c13.3125-.519532 24-11.4375 24-24.871094 0-6.574219-2.664062-13.007812-7.3125-17.65625l-13.34375-13.34375 13.425781-19.175781c4.734375-6.757813 7.230469-14.695313 7.230469-22.945313zm-61.175781-53.421875 3.273437.464844-9.226562 24.597656-9.558594-9.550781zm-10.824219-11.800781-16 16-16-16v-7.046875c5.070312 1.519531 10.441406 2.359375 16 2.359375s10.929688-.839844 16-2.359375zm-328 299.3125h224v16h-224zm224-16h-224v-16h224zm-144-96h-112v-80h112zm144-80h-32v-32h32zm0-68.246094v20.246094h-32v-31.96875l33.929688.015625c-1.167969 3.738281-1.929688 7.632813-1.929688 11.707031zm-48 20.246094h-48v-32l48 .023438zm-48 16h48v32h-48zm0 48h48v32h-48zm0 48h48v32h-48zm0 48h48v32h-48zm64 32v-32h61.214844l2.875 32zm59.785156-48h-59.785156v-32h56c.3125 0 .601562-.078125.902344-.089844zm-59.785156-48v-32h32v24c0 2.816406.574219 5.488281 1.472656 8zm43.414062-143.945312-123.414062-.054688v240h-16v-256h184v1.0625l-21.65625 3.097656c-9.023438 1.289063-16.929688 5.6875-22.929688 11.894532zm49.761719.433593 15.511719 15.511719-9.558594 9.550781-9.226562-24.597656zm66.824219-104.488281v8h-22.558594l5.160156-30.960938c10.015626 2.890626 17.398438 12.03125 17.398438 22.960938zm-46.558594-24h13.109375l-5.335937 32h-2.445313zm-33.441406 24c0-10.929688 7.382812-20.070312 17.398438-22.960938l5.160156 30.960938h-22.558594zm0 32v-8h80v8c0 22.054688-17.945312 40-40 40s-40-17.945312-40-40zm-240-64c0-4.414062 3.59375-8 8-8h160c4.40625 0 8 3.585938 8 8v104h-128v48h-48zm-112 168h160v16h-160zm16 32h144v16h-128v112h128v48h-89.886719l-54.113281 27.054688zm48 196.945312v38.109376l-38.113281-19.054688zm256 43.054688v-48h17.519531l4.304688 48zm24.152344-150.367188c.566406-.519531 1.09375-1.066406 1.609375-1.632812h38.238281v168h-24.695312zm80.542968 166.367188h-24.695312v-168h7.785156c7.75 0 15.03125-3.800781 19.488282-10.152344l2.992187-4.28125 7.109375 7.113282c.816406.816406 1.738281 1.488281 2.640625 2.167968l.015625 4.417969zm36.679688-199.3125c1.664062 1.671875 2.625 3.984375 2.625 6.34375 0 4.945312-4.023438 8.96875-8.96875 8.96875-2.398438 0-4.648438-.929688-6.34375-2.625l-9.113281-9.109375 10.449219-14.929687zm2.625-66.777344c0 4.945313-1.503906 9.707032-4.335938 13.769532l-45.503906 65c-1.464844 2.082031-3.839844 3.320312-6.375 3.320312-4.289062 0-7.785156-3.496094-7.785156-7.785156v-2.957032c0-1.671874.550781-3.328124 1.550781-4.671874l43.242188-57.648438c2.070312-2.753906 3.207031-6.167969 3.207031-9.601562v-13.335938h-16v13.335938l-6.679688 8.902343c-4.542968-3.972656-10.25-6.238281-16.351562-6.238281-13.769531 0-24.96875 11.199219-24.96875 24.96875 0 6.574219 2.664062 13.007812 7.3125 17.65625l3.839844 3.832031-6.398438 8.527344c-3.066406 4.105469-4.753906 9.175781-4.753906 14.273437v2.957032c0 2.738281.558594 5.328125 1.414062 7.785156h-33.414062v-96h-16v96c0 4.414062-3.59375 8-8 8s-8-3.585938-8-8v-108.246094c0-11.867187 8.855469-22.082031 20.609375-23.753906l5.070313-.726562 19.191406 51.167968 27.128906-27.128906 27.128906 27.128906 19.191406-51.167968 5.078126.726562c11.746093 1.671875 20.601562 11.878906 20.601562 23.753906zm-61.375 33.402344c-1.664062-1.671875-2.625-3.984375-2.625-6.34375 0-4.945312 4.023438-8.96875 8.96875-8.96875 2.398438 0 4.648438.929688 6.335938 2.625l.414062.414062-10.871094 14.496094zm0 0"/><path d="m288 48h-144v64h144zm-64 16h16v32h-16zm-16 32h-16v-32h16zm-48-32h16v32h-16zm112 32h-16v-32h16zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Expedición</h3>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<!--- --->
						<br><br>
						<div class="featured-boxes featured-boxes-style-4 custom-featured-boxes-style-1">
							<h1 style="text-transform: uppercase; color: #9a111e" class="d-block custom-font-secondary font-weight-semibold appear-animation text-center" data-appear-animation="maskUp">Construción e Instalación</h1>
							<div class="row mb-2" style="justify-content:center">
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg id="Layer_5" enable-background="new 0 0 64 64" height="512" viewBox="0 0 64 64" width="512" xmlns="http://www.w3.org/2000/svg"><path id="Shape" d="m49.69 12.281c-3.7761126-4.9636526-10.8556984-5.93795503-15.8322481-2.178856-4.9765498 3.7590991-5.9750811 10.8353082-2.2330397 15.8246968 3.7420415 4.9893887 10.814791 6.0121371 15.8169601 2.2871971 5.002169-3.7249401 6.0491227-10.7941471 2.3413277-15.8090379-.0278353-.0436148-.0589236-.085066-.093-.124zm-8.69 15.719c-4.9705627 0-9-4.0294373-9-9s4.0294373-9 9-9 9 4.0294373 9 9c-.0055111 4.9682782-4.0317218 8.9944889-9 9z"/><path id="Shape" d="m5 4c-.55228475 0-1 .44771525-1 1v2c0 .55228475.44771525 1 1 1s1-.44771525 1-1v-2c0-.55228475-.44771525-1-1-1z"/><path id="Shape" d="m10 7v-2c0-.55228475-.44771525-1-1-1s-1 .44771525-1 1v2c0 .55228475.44771525 1 1 1s1-.44771525 1-1z"/><path id="Shape" d="m13 8c.5522847 0 1-.44771525 1-1v-2c0-.55228475-.4477153-1-1-1s-1 .44771525-1 1v2c0 .55228475.4477153 1 1 1z"/><path id="Shape" d="m17 8c.5522847 0 1-.44771525 1-1v-2c0-.55228475-.4477153-1-1-1s-1 .44771525-1 1v2c0 .55228475.4477153 1 1 1z"/><path id="Shape" d="m58.632 15.823-3.268-1.089c-.2961432-.9965839-.6956473-1.9594825-1.192-2.873l1.542-3.083c.3868507-.76954099.2355866-1.70052097-.375-2.308l-1.809-1.811c-.6087604-.60820893-1.538321-.75883963-2.308-.374l-3.084 1.542c-.9132805-.49585843-1.8758304-.89502172-2.872-1.191l-1.089-3.269c-.2724905-.81669035-1.0370505-1.3673508-1.898-1.367h-2.558c-.8616854-.00121253-1.6272771.54961057-1.9 1.367l-1.087 3.269c-.9961696.29597828-1.9587195.69514157-2.872 1.191l-3.084-1.542c-.769541-.38685071-1.700521-.23558663-2.308.375l-1.808 1.809c-.3949119.40168311-.5994065.9529453-.562 1.515-.9894446-.03492892-1.8979436-.55578173-2.428-1.392l-2.647-4.247c-.9158382-1.46150322-2.5202555-2.34780677-4.245-2.345h-11.78c-2.76005315.00330612-4.99669388 2.23994685-5 5v50c.00330612 2.7600532 2.23994685 4.9966939 5 5h41.14c2.1389444.0004912 4.0403089-1.3622807 4.727-3.388l8.922-26.2c.4426715-1.4933582.1624822-3.1077715-.7575034-4.3646339-.9199857-1.2568625-2.3741878-2.0119335-3.9314966-2.0413661.087-.247.187-.489.262-.74l3.268-1.089c.8178759-.27145 1.369921-1.0362541 1.37-1.898v-2.558c.0002074-.8612148-.5509145-1.6258561-1.368-1.898zm-3.632 10.177c.9373377.0019182 1.8199066.4418377 2.3856959 1.1891584s.7497629 1.7161399.4973041 2.6188416l-8.91 26.161c-.4114781 1.2141825-1.5509885 2.0311065-2.833 2.031h-41.14c-1.65685425 0-3-1.3431458-3-3v-50c0-1.65685425 1.34314575-3 3-3h11.78c1.0359743-.00093826 1.9996278.53094104 2.551 1.408l2.644 4.247c.9158382 1.46150322 2.5202555 2.3478068 4.245 2.345h.68l.931 1.861c-.4963527.9135175-.8958568 1.8764161-1.192 2.873l-3.268 1.089c-.118757.047939-.2326168.107213-.34.177h-5.691c-2.2026954.0045411-4.1448005 1.4453007-4.788 3.552l-1.89 5.62c-.1756638.5232734.1058676 1.0899168.629 1.266.1027536.0347442.2105317.0523131.319.052.4294448.0001053.8110071-.2739917.948-.681l1.9-5.657c.3818805-1.2737662 1.5522278-2.1476687 2.882-2.152h4.66v2.279c-.0012125.8616854.5496106 1.6272771 1.367 1.9l3.269 1.09c.074.249.173.489.26.734h-5.556c-2.202422.0044063-4.1444411 1.4446966-4.788 3.551l-7.8 23.13c-.17566382.5232734.10586756 1.0899168.629 1.266.10287196.0340472.21064424.0509394.319.05.4294448.0001053.8110071-.2739917.948-.681l7.811-23.167c.3817731-1.2734134 1.551598-2.1472291 2.881-2.152h6.421c.024.045.042.094.067.139l-1.542 3.083c-.3845851.7698874-.2335473 1.6994751.375 2.308l1.809 1.811c.6089958.6078707 1.5385294.7580943 2.308.373l3.083-1.541c.9137119.4956858 1.8765748.89484 2.873 1.191l1.089 3.269c.2724905.8166903 1.0370505 1.3673508 1.898 1.367h2.558c.8616854.0012125 1.6272771-.5496106 1.9-1.367l1.089-3.269c.9964252-.29616 1.9592881-.6953142 2.873-1.191l3.081 1.54c.7696344.3883386 1.7017041.2378355 2.31-.373l1.809-1.81c.6085473-.6085249.7595851-1.5381126.375-2.308l-1.542-3.083c.025-.045.043-.094.067-.139zm3-5.721-3.773 1.258c-.3220811.1075077-.5676038.3711058-.652.7-.3050382 1.1816472-.7764152 2.3139621-1.4 3.363-.1732633.2924838-.1860701.6529579-.034.957l1.78 3.56-1.809 1.809-3.56-1.779c-.3037728-.1517053-.6637718-.138902-.956.034-1.0498284.6212928-2.1824289 1.0906285-3.364 1.394-.3291294.0845885-.5927766.3305336-.7.653l-1.253 3.772h-2.558l-1.258-3.773c-.1072234-.3224664-.3708706-.5684115-.7-.653-1.1812254-.3034607-2.3134835-.772794-3.363-1.394-.2922282-.172902-.6522272-.1857053-.956-.034l-3.56 1.78-1.809-1.81 1.78-3.56c.1520701-.3040421.1392633-.6645162-.034-.957-.188-.317-.357-.644-.517-.975l-.013-.029c-.3648211-.7562463-.6548662-1.5463344-.866-2.359-.0843962-.3288942-.3299189-.5924923-.652-.7l-3.773-1.257v-2.558l3.773-1.258c.3220811-.1075077.5676038-.3711058.652-.7.3050382-1.1816472.7764152-2.3139621 1.4-3.363.1732633-.2924838.1860701-.6529579.034-.957l-1.78-3.56 1.809-1.81 3.56 1.78c.3025262.15055596.6606251.13814287.952-.033 1.0498284-.62129281 2.1824289-1.09062845 3.364-1.394.3291294-.08458847.5927766-.33053365.7-.653l1.257-3.773h2.558l1.258 3.773c.1072234.32246635.3708706.56841153.7.653 1.1812254.30346072 2.3134835.77279396 3.363 1.394.2922282.17290199.6522272.1857053.956.034l3.56-1.78 1.809 1.81-1.78 3.56c-.1520701.3040421-.1392633.6645162.034.957.6235848 1.0490379 1.0949618 2.1813528 1.4 3.363.0843962.3288942.3299189.5924923.652.7l3.769 1.257z"/><path id="Shape" d="m41 15c-2.209139 0-4 1.790861-4 4s1.790861 4 4 4 4-1.790861 4-4-1.790861-4-4-4zm0 6c-1.1045695 0-2-.8954305-2-2s.8954305-2 2-2 2 .8954305 2 2-.8954305 2-2 2z"/></g></g></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3">Gerencia de Proyecto</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="512" height="512"><g id="Consult-Talk"><rect x="16" y="25" width="2" height="2"/><rect x="26" y="25" width="2" height="2"/><path d="M61.142,36.01l-3.907-.558a14,14,0,0,0-.657-1.6L58.942,30.7a1,1,0,0,0-.093-1.307l-4.243-4.243A.989.989,0,0,0,54,24.867V15.241A8,8,0,0,0,57,9a7.917,7.917,0,0,0-4.573-7.224A1,1,0,0,0,51,2.68v5.6l-2,.667-2-.667V2.68a1,1,0,0,0-1.427-.9A7.917,7.917,0,0,0,41,9a8,8,0,0,0,3,6.241V22H41a1,1,0,0,0-.99.858l-.647,4.529,1.98.283L41.867,24h4.266l.524,3.67a1,1,0,0,0,.71.818,11.939,11.939,0,0,1,2.4.987,1,1,0,0,0,1.081-.077l2.961-2.22,3.016,3.016L54.6,33.155a1,1,0,0,0-.077,1.081,11.939,11.939,0,0,1,.987,2.4,1,1,0,0,0,.818.71l3.67.524v4.266l-3.67.524a1,1,0,0,0-.818.71,11.939,11.939,0,0,1-.987,2.4,1,1,0,0,0,.077,1.081l2.22,2.961-3.016,3.016L50.845,50.6a1,1,0,0,0-1.081-.077,11.939,11.939,0,0,1-2.4.987,1,1,0,0,0-.71.818L46.133,56H42V51.771a11.1,11.1,0,0,0-.45-3.121,9.11,9.11,0,1,0-6.394-7.062c-.024-.01-.047-.022-.071-.031L27,38.323V36.786A11.089,11.089,0,0,0,30.479,34H33a3,3,0,0,0,3-3V28.816A3,3,0,0,0,38,26V23a3,3,0,0,0-2-2.816V19A14,14,0,0,0,8,19v1.184A3,3,0,0,0,6,23v3a3,3,0,0,0,3,3h2c.064,0,.126-.01.189-.014A11.023,11.023,0,0,0,17,36.786v1.537L8.915,41.557A10.949,10.949,0,0,0,2,51.771V61a1,1,0,0,0,1,1H41a1,1,0,0,0,1-1V58h5a1,1,0,0,0,.99-.858l.558-3.907a14,14,0,0,0,1.6-.657L53.3,54.942a1,1,0,0,0,1.307-.093l4.243-4.243a1,1,0,0,0,.093-1.307l-2.364-3.152a14,14,0,0,0,.657-1.6l3.907-.558A1,1,0,0,0,62,43V37A1,1,0,0,0,61.142,36.01Zm-11-8.588c-.047-.023-.1-.038-.147-.06V15H48v7.93l-.01-.072A1,1,0,0,0,47,22H46V14.74a1,1,0,0,0-.429-.821A6,6,0,0,1,45,4.522V9a1,1,0,0,0,.684.949l3,1a1,1,0,0,0,.632,0l3-1A1,1,0,0,0,53,9V4.522a6,6,0,0,1-.571,9.4A1,1,0,0,0,52,14.74V26.033ZM37,40a7.05,7.05,0,1,1,3.333,5.941,10.916,10.916,0,0,0-2.611-2.877A6.836,6.836,0,0,1,37,40Zm-4-8H31.786a10.9,10.9,0,0,0,1.025-3.014c.063,0,.125.014.189.014h1v2A1,1,0,0,1,33,32ZM31,21a3.015,3.015,0,0,1-2.4-1.2l-1.8-2.4a1,1,0,0,0-.674-.392,1.011,1.011,0,0,0-.751.211L22.019,19.9A5.018,5.018,0,0,1,18.9,21H13V20a9,9,0,0,1,18,0Zm5,5a1,1,0,0,1-1,1H33V22h2a1,1,0,0,1,1,1ZM22,7A12.013,12.013,0,0,1,34,19v1H33a11,11,0,0,0-22,0H10V19A12.013,12.013,0,0,1,22,7ZM9,27a1,1,0,0,1-1-1V23a1,1,0,0,1,1-1h2v5Zm4,0V23h5.9a7.024,7.024,0,0,0,4.372-1.534l2.551-2.041L27,21a5.025,5.025,0,0,0,4,2v4a8.947,8.947,0,0,1-1.522,5H22v2h5.644A8.991,8.991,0,0,1,13,27Zm9,11a10.966,10.966,0,0,0,3-.426V39a3,3,0,0,1-6,0V37.574A10.966,10.966,0,0,0,22,38ZM40,60H34V49H32V60H12V49H10V60H4V51.771a8.958,8.958,0,0,1,5.658-8.357l7.563-3.025a4.968,4.968,0,0,0,9.558,0l7.563,3.025A8.958,8.958,0,0,1,40,51.771Z"/><rect x="43" y="26" width="2" height="2"/><rect x="43" y="52" width="2" height="2"/><rect x="52.192" y="48.192" width="2" height="2" transform="translate(-19.205 52.021) rotate(-45)"/><rect x="56" y="39" width="2" height="2"/><rect x="52.192" y="29.808" width="2" height="2" transform="translate(-6.205 46.636) rotate(-45)"/></g></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Asistencia Técnica</h3>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-6 col-xl-3">
										<div class="featured-box featured-box-primary featured-box-effect-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
											<div class="box-content">
												<svg height="496pt" viewBox="0 0 496 496" width="496pt" xmlns="http://www.w3.org/2000/svg"><path d="m496 213.910156v-26.167968c0-19.773438-14.769531-36.796876-34.335938-39.589844l-21.664062-3.089844v-11.175781c14.488281-10.132813 24-26.910157 24-45.886719v-8h16v-16h-16v-8c0-22.054688-17.945312-40-40-40h-32c-22.054688 0-40 17.945312-40 40v8h-16v16h16v8c0 15.664062 6.480469 29.824219 16.886719 40h-48.886719v-104c0-13.230469-10.769531-24-24-24h-160c-13.230469 0-24 10.769531-24 24v152h-112v48h16v211.054688l-9.886719 4.945312 80 40h273.144531l1.429688 16h94.609375l16.703125-184v-.097656c13.3125-.519532 24-11.4375 24-24.871094 0-6.574219-2.664062-13.007812-7.3125-17.65625l-13.34375-13.34375 13.425781-19.175781c4.734375-6.757813 7.230469-14.695313 7.230469-22.945313zm-61.175781-53.421875 3.273437.464844-9.226562 24.597656-9.558594-9.550781zm-10.824219-11.800781-16 16-16-16v-7.046875c5.070312 1.519531 10.441406 2.359375 16 2.359375s10.929688-.839844 16-2.359375zm-328 299.3125h224v16h-224zm224-16h-224v-16h224zm-144-96h-112v-80h112zm144-80h-32v-32h32zm0-68.246094v20.246094h-32v-31.96875l33.929688.015625c-1.167969 3.738281-1.929688 7.632813-1.929688 11.707031zm-48 20.246094h-48v-32l48 .023438zm-48 16h48v32h-48zm0 48h48v32h-48zm0 48h48v32h-48zm0 48h48v32h-48zm64 32v-32h61.214844l2.875 32zm59.785156-48h-59.785156v-32h56c.3125 0 .601562-.078125.902344-.089844zm-59.785156-48v-32h32v24c0 2.816406.574219 5.488281 1.472656 8zm43.414062-143.945312-123.414062-.054688v240h-16v-256h184v1.0625l-21.65625 3.097656c-9.023438 1.289063-16.929688 5.6875-22.929688 11.894532zm49.761719.433593 15.511719 15.511719-9.558594 9.550781-9.226562-24.597656zm66.824219-104.488281v8h-22.558594l5.160156-30.960938c10.015626 2.890626 17.398438 12.03125 17.398438 22.960938zm-46.558594-24h13.109375l-5.335937 32h-2.445313zm-33.441406 24c0-10.929688 7.382812-20.070312 17.398438-22.960938l5.160156 30.960938h-22.558594zm0 32v-8h80v8c0 22.054688-17.945312 40-40 40s-40-17.945312-40-40zm-240-64c0-4.414062 3.59375-8 8-8h160c4.40625 0 8 3.585938 8 8v104h-128v48h-48zm-112 168h160v16h-160zm16 32h144v16h-128v112h128v48h-89.886719l-54.113281 27.054688zm48 196.945312v38.109376l-38.113281-19.054688zm256 43.054688v-48h17.519531l4.304688 48zm24.152344-150.367188c.566406-.519531 1.09375-1.066406 1.609375-1.632812h38.238281v168h-24.695312zm80.542968 166.367188h-24.695312v-168h7.785156c7.75 0 15.03125-3.800781 19.488282-10.152344l2.992187-4.28125 7.109375 7.113282c.816406.816406 1.738281 1.488281 2.640625 2.167968l.015625 4.417969zm36.679688-199.3125c1.664062 1.671875 2.625 3.984375 2.625 6.34375 0 4.945312-4.023438 8.96875-8.96875 8.96875-2.398438 0-4.648438-.929688-6.34375-2.625l-9.113281-9.109375 10.449219-14.929687zm2.625-66.777344c0 4.945313-1.503906 9.707032-4.335938 13.769532l-45.503906 65c-1.464844 2.082031-3.839844 3.320312-6.375 3.320312-4.289062 0-7.785156-3.496094-7.785156-7.785156v-2.957032c0-1.671874.550781-3.328124 1.550781-4.671874l43.242188-57.648438c2.070312-2.753906 3.207031-6.167969 3.207031-9.601562v-13.335938h-16v13.335938l-6.679688 8.902343c-4.542968-3.972656-10.25-6.238281-16.351562-6.238281-13.769531 0-24.96875 11.199219-24.96875 24.96875 0 6.574219 2.664062 13.007812 7.3125 17.65625l3.839844 3.832031-6.398438 8.527344c-3.066406 4.105469-4.753906 9.175781-4.753906 14.273437v2.957032c0 2.738281.558594 5.328125 1.414062 7.785156h-33.414062v-96h-16v96c0 4.414062-3.59375 8-8 8s-8-3.585938-8-8v-108.246094c0-11.867187 8.855469-22.082031 20.609375-23.753906l5.070313-.726562 19.191406 51.167968 27.128906-27.128906 27.128906 27.128906 19.191406-51.167968 5.078126.726562c11.746093 1.671875 20.601562 11.878906 20.601562 23.753906zm-61.375 33.402344c-1.664062-1.671875-2.625-3.984375-2.625-6.34375 0-4.945312 4.023438-8.96875 8.96875-8.96875 2.398438 0 4.648438.929688 6.335938 2.625l.414062.414062-10.871094 14.496094zm0 0"/><path d="m288 48h-144v64h144zm-64 16h16v32h-16zm-16 32h-16v-32h16zm-48-32h16v32h-16zm112 32h-16v-32h16zm0 0"/></svg>
												<h3 class="font-weight-bold text-color-light text-5 text-capitalize ls-0 my-3"> Supervisión de Obra</h3>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
                        <!--- --->
						<div class="row">
							<div class="col text-center">
								<a data-hash data-hash-offset="95" href="#contactus" class="btn btn-dark btn-outline custom-btn-color-1 font-weight-extra-bold text-color-light text-3 px-5 py-3 border-width-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" style="color: white !important;">CONTACTO</a>
							</div>
						</div>
					</div>		
				</section>
                <!--
				<section class="section section-height-4 bg-light border-0 m-0" id="ourprojects">
					<div class="container container-xl">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-xl-6 text-center">
								<div class="overflow-hidden">
									<span class="d-block text-color-primary custom-font-secondary font-weight-semibold appear-animation" data-appear-animation="maskUp">THE PORTFOLIO</span>
								</div>
								<div class="overflow-hidden mb-3">
									<h2 class="text-color-dark font-weight-bold mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="250">Our Projects</h2>
								</div>
								<p class="text-4 mb-5 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="500">Lorem ipsum dolor sit amet, consectetur adipiscing eli blandit massa im. Nullam id varius nunc. Vivamus bibendum mex, et faucibus lacus venena.</p>
							</div>
						</div>
						
						<div id="portfolioLoadMoreWrapper" class="row portfolio-list sort-destination mb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" data-sort-id="portfolio" data-total-pages="3" data-ajax-url="ajax/demo-architecture-interior-ajax-load-more-">

							<div class="col-lg-4 isotope-item">
								<div class="portfolio-item">
									<a href="demo-architecture-interior-projects-detail.html" class="text-decoration-none">
										<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-dark-linear custom-thumb-info-style-1">
											<span class="thumb-info-wrapper">
												<img src="<?php echo base_url(); ?>assets/img/bdn.jpeg" class="img-fluid" alt="">
												<span class="thumb-info-title text-left">
													<span class="thumb-info-inner font-weight-bold line-height-1 text-4 mb-3">Project Name</span>
													<span class="thumb-info-type text-transform-none font-weight-light text-1 line-height-7 pr-xl-5 mr-5">Lorem ipsum dolor sit amet, consectetur adipiscing eli blandit massa im. </span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<div class="col-lg-4 isotope-item">
								<div class="portfolio-item">
									<a href="demo-architecture-interior-projects-detail.html" class="text-decoration-none">
										<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-dark-linear custom-thumb-info-style-1">
											<span class="thumb-info-wrapper">
												<img src="<?php echo base_url(); ?>assets/img/fg.jpeg" class="img-fluid" alt="">
												<span class="thumb-info-title text-left">
													<span class="thumb-info-inner font-weight-bold line-height-1 text-4 mb-3">Project Name</span>
													<span class="thumb-info-type text-transform-none font-weight-light text-1 line-height-7 pr-xl-5 mr-5">Lorem ipsum dolor sit amet, consectetur adipiscing eli blandit massa im. </span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>
							<div class="col-lg-4 isotope-item">
								<div class="portfolio-item">
									<a href="demo-architecture-interior-projects-detail.html" class="text-decoration-none">
										<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-dark-linear custom-thumb-info-style-1">
											<span class="thumb-info-wrapper">
												<img src="<?php echo base_url(); ?>assets/img/peed.jpeg" class="img-fluid" alt="">
												<span class="thumb-info-title text-left">
													<span class="thumb-info-inner font-weight-bold line-height-1 text-4 mb-3">Project Name</span>
													<span class="thumb-info-type text-transform-none font-weight-light text-1 line-height-7 pr-xl-5 mr-lg-5">Lorem ipsum dolor sit amet, consectetur adipiscing eli blandit massa im. </span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</div>

						</div>

						<div id="portfolioLoadMoreBtnWrapper" class="row">
							<div class="col text-center">

								<div id="portfolioLoadMoreLoader" class="portfolio-load-more-loader" style="min-height: 61px;">
									<div class="bounce-loader">
										<div class="bounce1"></div>
										<div class="bounce2"></div>
										<div class="bounce3"></div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</section>
                -->
				<section class="section section-parallax section-height-4 overlay overlay-show border-0 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?php echo base_url(); ?>assets/img/muuk/tubos.jpeg" id="whyhireus">
					<div class="container container-xl pb-5 mb-4">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-xl-6 text-center">
								<h2 class="text-color-light font-weight-bold pb-3 mb-3">ALGUNOS DE NUESTROS CLIENTES</h2>
							</div>
						</div>
					</div>
				</section>
				<div class="container container-xl custom-negative-margin-top z-index-2 position-relative">
					<div class="row align-items-center justify-content-center">
						<div class="col-sm-6 col-lg-3 col-xl-2 order-2 order-xl-1 mb-4 mb-lg-0">
							<div class="card border-0 custom-box-shadow-1" style="background-color: #43474a !important;">
								<div class="card-body my-4 my-xl-5">
									<div class="custom-content-rotator">
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/mond.png" class="img-fluid" alt="" /></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 col-xl-2 order-3 order-xl-2 mb-4 mb-lg-0">
							<div class="card border-0 custom-box-shadow-1" style="background-color: #43474a !important;">
								<div class="card-body my-4 my-xl-5">
									<div class="custom-content-rotator">
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/petrotiger.png" class="img-fluid" alt="" /></div>
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/schl.png" class="img-fluid" alt="" /></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-8 col-xl-4 order-1 order-xl-3 mx-lg-5 mx-xl-0 mb-5">
							<div class="card border-0 custom-box-shadow-1" style="background-color: #43474a !important;">
								<div class="card-body text-center mt-4">
									<div class="owl-carousel owl-theme nav-style-1 nav-dark custom-nav-size-1 mb-0" data-plugin-options="{'items':1, 'nav': true, 'dots': false}">
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/mond.png" class="img-fluid" alt="" />
										</div>
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/petrotiger.png" class="img-fluid" alt="" />
										</div>
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/schl.png" class="img-fluid" alt="" />
										</div>
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/imp.png" class="img-fluid" alt="" />
										</div>
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/pemex_e.png" class="img-fluid" alt="" />
										</div>
										<div class="text-center px-5">
											<img src="<?php echo base_url(); ?>assets/img/clientes/udlap.png" class="img-fluid" alt="" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 col-xl-2 order-4 order-xl-4 mb-4 mb-sm-0">
							<div class="card border-0 custom-box-shadow-1" style="background-color: #43474a !important;">
								<div class="card-body my-4 my-xl-5">
									<div class="custom-content-rotator">
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/imp.png" class="img-fluid" alt="" /></div>
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/pemex_e.png" class="img-fluid" alt="" /></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 col-xl-2 order-5 order-xl-5 mb-4 mb-sm-0">
							<div class="card border-0 custom-box-shadow-1" style="background-color: #43474a !important;">
								<div class="card-body my-4 my-xl-5">
									<div class="custom-content-rotator">
										<div><img src="<?php echo base_url(); ?>assets/img/clientes/udlap.png" class="img-fluid" alt="" /></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<section style="background-color: #818181 !important;" class="section section-height-4 border-0 m-0" id="contactus">
					<div class="container container-xl">
						<div class="row mb-5">
							<div class="col">
								<h2 class="text-color-light font-weight-bold mb-3" style="color: white !important;">Contacto</h2>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-lg-12 col-xl-12 order-2 order-xl-1 mb-4 mb-lg-0">
								<div class="card custom-card-style-1 h-100">
									<div class="card-body">
									    <h4 style="color: white !important;">PUEBLA, CDMX Y CD. DEL CARMEN</h4>
										<ul class="list list-icons list-icons-sm">

									        <li class="text-color-light font-weight-light">
												<i class="fas fa-angle-right custom-text-color-grey-1"></i> <strong class="font-weight-normal" style="color: white !important; font-size: 18px !important;">Email:</strong><a href="mailto:mail@example.com" class="link-hover-style-1 ml-1" style="color: white !important;  font-size: 18px !important;"> ventas@iknalmuuk.com</a>
											</li>	
											<li class="text-color-light font-weight-light">
												<i class="fas fa-angle-right custom-text-color-grey-1"></i> <strong class="font-weight-normal" style="color: white !important;  font-size: 18px !important;">Tel:</strong><a class="link-hover-style-1 ml-1" style="color: white !important;  font-size: 18px !important;"> 55 48997623</a>
											</li>
											<li class="text-color-light font-weight-light">
												<i class="fas fa-angle-right custom-text-color-grey-1"></i> <strong class="font-weight-normal" style="color: white !important; font-size: 18px !important;">Tel:</strong><a class="link-hover-style-1 ml-1" style="color: white !important; font-size: 18px !important;">221 598 3208</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div> 

			<footer id="footer" class="bg-quaternary border-width-1 custom-border-color-grey py-5 mt-0">
				<div class="container container-xl">
					<div class="row justify-content-between">
						<div class="col-auto">
							<ul class="social-icons social-icons-clean social-icons-icon-light">
								<li class="social-icons-facebook"><a href="https://www.facebook.com/IKNALMUUK/" target="_blank" title="Facebook"><i class="fab fa-facebook-f" style="font-size: 25px;"></i></a></li>
								<li class="social-icons-instagram"><a href="https://www.instagram.com/iknalmuuk/" target="_blank" title="Instagram"><i class="fab fa-instagram" style="font-size: 25px;"></i></a></li>
							</ul>
						</div>
						<div class="col-auto text-right">
							<p class="text-color-light font-weight-light opacity-8 mb-0" style="color: white !important;">Copyrights © 2020 Todos los derechos reservado por BeaverDS</p>
						</div>
					</div>
				</div>
			</footer>

		</div>